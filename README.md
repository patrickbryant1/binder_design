# A pipeline for designing peptide binders


Binder design using a combination of [Foldseek](https://search.foldseek.com), [ESM-IF1](https://www.biorxiv.org/content/10.1101/2022.04.10.487779v2) and [AlphaFold](https://www.nature.com/articles/s41586-021-03819-2).

Foldseek is available under [GNU GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). \
ESM-IF1 is available under the [MIT license](https://opensource.org/licenses/MIT). \
AlphaFold2 is available under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0). \
The AlphaFold2 parameters are made available under the terms of the [CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/legalcode) and have not been modified.


The pipeline here is available under the same licenses as a derivative of these methods.  

**You may not use these files except in compliance with the licenses.**
