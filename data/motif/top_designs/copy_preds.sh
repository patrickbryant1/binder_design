for i in {1..12}
do
	LINE=$(sed -n $i'p' ./best.txt)
	ID=$(echo $LINE|cut -d ',' -f 1)
	NUMBER=$(echo $LINE|cut -d ',' -f 2)
	echo $ID, $NUMBER
	cp /proj/berzelius-2021-29/users/x_patbr/results/binder/motif/$ID'_1/unrelaxed_'$NUMBER'.pdb' ./$ID'_design.pdb'
done
