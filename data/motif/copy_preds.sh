for file in  /proj/berzelius-2021-29/users/x_patbr/results/binder/motif/*_1/sequence.npy
do
	ID=$(echo $file | cut -d '/' -f 9)
	cp $file ./$ID'_sequence.npy'
	cp /proj/berzelius-2021-29/users/x_patbr/results/binder/motif/$ID'/loss.npy' ./$ID'_loss.npy'
done
