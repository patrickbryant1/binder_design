BASE=/proj/berzelius-2021-29/users/x_patbr/results/binder/minibinder
for i in {1..4}
do
	LINE=$(sed -n $i'p' best_worst.txt)
	ID=$(echo $LINE| cut -d ',' -f 1)
	echo $ID
	BEST=$(echo $LINE| cut -d ',' -f 2)
	WORST=$(echo $LINE| cut -d ',' -f 4)
	cp $BASE/$ID'_'$BEST/unrelaxed_true.pdb $ID'_best.pdb'
	cp $BASE/$ID'_'$WORST/unrelaxed_true.pdb $ID'_worst.pdb'
done
