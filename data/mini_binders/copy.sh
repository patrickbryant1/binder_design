BASE=/home/patrick/data/41586_2022_4654_MOESM4_ESM/supplemental_files/sorting_ngs_data
IDS=./ids.txt
for i in {1..13}
do
  ID=$(sed -n $i'p' $IDS)
  echo $ID
  cp $BASE/$ID'_ssm/sequences.list' $ID'_seqs.list'
  cp $BASE/$ID'_ssm/pooled_counts.list' $ID'_counts.list'
done
