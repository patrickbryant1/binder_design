#Get a sequence design
CROPDF=../data/Pfam/crops/1T0P_A-1T0P_B.csv
NSEQS=1
STRUCTURE=../data/Pfam/PDB/pairs/1T0P_A-1T0P_B.pdb
TEMP=0.000001
CROPLEN=0
OUTNAME=../data/Pfam/PDB/designs/1T0P_A-1T0P_B.csv
python3 ./esm_if1_pred.py --crop_df $CROPDF \
--n_seqs_per_crop $NSEQS \
--temperature $TEMP \
--crop_len $CROPLEN \
--structure $STRUCTURE --outname $OUTNAME

# #Predict with AF_mod
# ### common portion to all configuration paths
# COMMON='/proj/berzelius-2021-29/'
# ### Local path of alphafold directory
# AFHOME=$COMMON'/users/x_patbr/binder_design/src'
# #Singularity image
# IMG=/proj/berzelius-2021-29/users/x_patbr/singularity_imgs/af_torch_sbox
# ### path of param folder containing AF2 Neural Net parameters.
# ### download from: https://storage.googleapis.com/alphafold/alphafold_params_2021-07-14.tar)
# PARAMS=$COMMON'/Database/af_params/'
#
# ### Path where AF2 generates its output folder structure
# OUTFOLDER=../data/Pfam/PDB/designs/structure_pred/
#
# ### Running options for obtaining a refines tructure ###
# MAX_RECYCLES=8 #max_recycles (default=3)
# MODEL_NAME='model_1' #model_1_ptm
#
# #Chain to design towards
# ID=1T0P_A
# #Get fasta
# FASTADIR=../data/Pfam/fasta/
# RECEPTORFASTA=$FASTADIR/$ID.fasta
#
# #Get MSA
# MSADIR=/proj/berzelius-2021-29/users/x_patbr/data/msas/binder/hhblits/Pfam
# MSAS=$MSADIR/$ID.a3m #Comma separated list of msa paths
#
# echo $RECEPTORFASTA, $MSAS
#
# #Predict designs towards chain A
# DESIGNS=../data/Pfam/PDB/designs/1T0P_A-1T0P_B.csv
# SEQS=$(tail -n 5 $DESIGNS|cut -d ',' -f 7)
# SEQS=$(echo "$SEQS" |paste -s -d ",")
#
# singularity exec --nv $IMG \
# python3 $AFHOME/af_mod_pred.py \
# 		--receptor_fasta_path=$RECEPTORFASTA \
# 		--model_names=$MODEL_NAME \
# 		--output_dir=$OUTFOLDER \
# 		--max_recycles=$MAX_RECYCLES \
# 		--msas=$MSAS \
# 		--peptide_sequences=$SEQS \
# 	  	--data_dir=$PARAMS
#
#
# #Clean up
# rm -r $OUTFOLDER/$ID
