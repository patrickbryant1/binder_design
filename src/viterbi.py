
import sys
import os
import pdb

import numpy as np
import pandas as pd
import argparse
import glob
from collections import Counter


parser = argparse.ArgumentParser(description = '''Run the Viterbi algorithm on a profile created from the designs to obtain an optimal consensus.''')
parser.add_argument('--design_df', nargs=1, type= str, required=True, help = "Df with with designed seqs")

#################FUNCTIONS#################
def create_profile(sequences):
    """Create a sequence profile (HMM) using the sequences in the design df
    There are only transition profiles here representing going from the current state
    to any other
    """

    #Create seq array
    seq_array = []
    for seq in sequences:
        seq_array.append([x for x in seq])
    seq_array = np.array(seq_array).T
    #Mapping
    mapping = {'-': 20, 'A': 0, 'B': 20, 'C': 1, 'D': 2, 'E': 3, 'F': 4,
             'G': 5,'H': 6, 'I': 7, 'K': 8, 'L': 9, 'M': 10,'N': 11,
             'O': 20, 'P': 12,'Q': 13, 'R': 14, 'S': 15, 'T': 16,
             'V': 17, 'W': 18, 'Y': 19,'U': 20, 'Z': 20, 'X': 20, 'J': 20}
    n_res = len(sequences[0])
    profile = np.zeros((n_res, 20, 20)) #seqlen, current state, transition probability to each other state


    #Calc occ
    for i in range(len(seq_array)-1):
        #Get row and next row
        row = seq_array[i]
        next_row = seq_array[i+1]
        #Go through all residues
        for res in np.unique(row):
            res_pos = np.argwhere(row==res)[:,0]
            #Go through all transitions
            for pos in res_pos:
                profile[i+1,mapping[res],mapping[next_row[pos]]]+=1
    #Add the start prob
    for res in seq_array[0]:
        profile[0,0,mapping[res]]+=1

    #Add noise
    profile+=1e-6

    #Normalise
    for i in range(len(profile)):
        profile[i] /= profile[i].sum()
    #Create log prob
    profile = -np.log(profile)
    return profile

def Viterbi(profile):
    """Get the most likely sequence from the profile
    This is the minimum path length through the graph connecting all residues in the profile.

    By solving the shortest path to all possibilities at each time step, this is found quickly.
    """

    #profile: residue, current state, transition probability to each other state
    n_res = profile.shape[0]
    shortest_paths = np.zeros((n_res, 20, 2)) #res, state, (origin, tot_score)
    #Add the initial scores
    shortest_paths[0,:,1]=profile[0,0,:]

    #Forward
    #Go through all res
    for i in range(1,len(profile)):
        #Get the residue transition probabilities for each state
        res_i = profile[i]
        #Get the shortest paths to all states
        for j in range(len(res_i)):
            #Get the score for the path to this state: prev res, state, score
            #+the current transition prob = profile(prev_res,prev_state, current_state)
            scores_j = shortest_paths[i-1,:,1]+profile[i-1,:,j]
            #Save the best = min of the neg log transition prob
            shortest_paths[i,j] = [np.argmin(scores_j),min(scores_j)]

    #Backtrack to get the shortest path
    best_path = []
    best_score = np.argmin(shortest_paths[-1,:,1])
    best_path.append(best_score) #Final residue
    best_path.append(int(shortest_paths[-1][best_score,0])) #Origin of final residue
    for i in range(1,len(shortest_paths)-1):
        next_transition = shortest_paths[-i][best_path[-1]][0]
        best_path.append(int(next_transition))

    #Invert
    best_path = best_path[::-1]
    #Back map
    back_mapping = {20: 'J', 0: 'A', 1: 'C', 2: 'D', 3: 'E', 4: 'F', 5: 'G',
                    6: 'H', 7: 'I', 8: 'K', 9: 'L', 10: 'M', 11: 'N', 12: 'P',
                    13: 'Q', 14: 'R', 15: 'S', 16: 'T', 17: 'V', 18: 'W', 19: 'Y'}
    best_seq = []
    for res in best_path:
        best_seq.append(back_mapping[res])

    return ''.join(best_seq), np.min(shortest_paths[-1,:,1])

#################MAIN####################

#Parse args
args = parser.parse_args()
#Get data
design_df = pd.read_csv(args.design_df[0])

viterbi_seqs, viterbi_scores = [], []
for ind, row in design_df.iterrows():
    #Create profile
    profile = create_profile(row.designed_binder_seq.split('-'))
    #Run Viterbi to find the optimal sequence
    best_seq, score = Viterbi(profile)
    viterbi_seqs.append(best_seq)
    viterbi_scores.append(score)
design_df['Viterbi_seq']=viterbi_seqs
design_df['Viterbi_score']=viterbi_scores
#Save
design_df.to_csv(args.design_df[0], index=None)
