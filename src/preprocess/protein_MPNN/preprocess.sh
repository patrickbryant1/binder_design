
#Get the test interactions from the protein_MPNN training set
META=../../../data/protein_MPNN/protein_mpnn_test.csv
OUTDIR=../../../data/protein_MPNN/
#python3 ./extract_test.py --meta $META --outdir $OUTDIR

#Extract all pairs
META=../../../data/protein_MPNN/protein_mpnn_test_cleaned.csv
STRDIR=../../../data/protein_MPNN/PDB/
OUTDIR=../../../data/protein_MPNN/PDB/pairs/
# python3 ./write_pairs.py --meta $META \
# --structure_dir $STRDIR --outdir $OUTDIR

#Write fasta
META=../../../data/protein_MPNN/selected_ints.csv
STRDIR=../../../data/protein_MPNN/PDB/pairs/
FASTADIR=../../../data/protein_MPNN/fasta/
# python3 ./write_fasta.py --meta $META \
# --structure_dir $STRDIR --outdir $FASTADIR

#Crop
MINRES=10
MAXRES=10
OUTDIR=../../../data/protein_MPNN/crops/
python3 ./select_crop.py --meta $META \
--structure_dir $STRDIR \
--minres $MINRES --maxres $MAXRES \
--outdir $OUTDIR
