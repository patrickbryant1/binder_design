import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB import MMCIFParser
import pdb

parser = argparse.ArgumentParser(description = '''Write the fasta sequences for all interacting chains.''')

parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with interacting chains.')
parser.add_argument('--structure_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to PDB files.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    model_coords = {}
    model_seqs = {}
    model_atoms = {}
    model_resnos = {}

    for model in struc:
        for chain in model:
            #Save
            model_coords[chain.id]=[]
            model_seqs[chain.id]=[]
            model_atoms[chain.id]=[]
            model_resnos[chain.id]=[]

            #Save residue
            for residue in chain:
                res_name = residue.get_resname()
                if res_name not in three_to_one.keys():
                    continue
                for atom in residue:
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    #Save
                    model_coords[chain.id].append(atom.get_coord())
                    model_seqs[chain.id].append(res_name)
                    model_atoms[chain.id].append(atom_id)
                    model_resnos[chain.id].append(residue.get_id()[1])



    #Convert to array
    for key in model_coords:
        model_coords[key] = np.array(model_coords[key])
        model_seqs[key] = np.array(model_seqs[key])
        model_atoms[key] = np.array(model_atoms[key])
        model_resnos[key] = np.array(model_resnos[key])

    return model_coords, model_seqs, model_atoms, model_resnos


def write_fasta(atoms1, atoms2, seq1, seq2, outdir, id1, id2):
    """Write the interacting pair
    """


    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    seq1 = seq1[np.argwhere(atoms1=='CA')[:,0]]
    seq1 = [three_to_one[x] for x in seq1]
    seq1 = ''.join(seq1)
    #Write chain 1
    with open(outdir+id1+'.fasta', 'w') as file:
        file.write('>'+id1+'\n')
        file.write(seq1)

    seq2 = seq2[np.argwhere(atoms2=='CA')[:,0]]
    seq2 = [three_to_one[x] for x in seq2]
    seq2 = ''.join(seq2)
    #Write chain 2
    with open(outdir+id2+'.fasta', 'w') as file:
        file.write('>'+id2+'\n')
        file.write(seq2)

##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
meta = pd.read_csv(args.meta[0])
structure_dir = args.structure_dir[0]
outdir = args.outdir[0]

#Get all files
for ind, row in meta.iterrows():
    model_coords, model_seqs, model_atoms, model_resnos = read_pdb(structure_dir+row.PDBID+'_'+row.Chain1+'-'+row.PDBID+'_'+row.Chain2+'.pdb')

    #Write fasta
    write_fasta(model_atoms['A'], model_atoms['B'],
                model_seqs['A'], model_seqs['B'],
                outdir, row.PDBID+'_'+row.Chain1, row.PDBID+'_'+row.Chain2)
