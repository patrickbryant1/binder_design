import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict, Counter
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB import MMCIFParser
import pdb

parser = argparse.ArgumentParser(description = '''Select a crop of the interacting chains (CBs<8Å) to predict (scaffold-->ESM-IF1-->AF).''')

parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with interacting chains.')
parser.add_argument('--structure_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to PDB files.')
parser.add_argument('--minres', nargs=1, type= int, default=sys.stdin, help = 'Min interface segment length')
parser.add_argument('--maxres', nargs=1, type= int, default=sys.stdin, help = 'Max interface segment length')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    model_coords = {}
    model_seqs = {}
    model_atoms = {}
    model_resnos = {}

    for model in struc:
        for chain in model:
            #Save
            model_coords[chain.id]=[]
            model_seqs[chain.id]=[]
            model_atoms[chain.id]=[]
            model_resnos[chain.id]=[]

            #Save residue
            for residue in chain:
                res_name = residue.get_resname()
                if res_name not in three_to_one.keys():
                    continue
                for atom in residue:
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    #Save
                    model_coords[chain.id].append(atom.get_coord())
                    model_seqs[chain.id].append(res_name)
                    model_atoms[chain.id].append(atom_id)
                    model_resnos[chain.id].append(residue.get_id()[1])



    #Convert to array
    for key in model_coords:
        model_coords[key] = np.array(model_coords[key])
        model_seqs[key] = np.array(model_seqs[key])
        model_atoms[key] = np.array(model_atoms[key])
        model_resnos[key] = np.array(model_resnos[key])

    return model_coords, model_seqs, model_atoms, model_resnos

def get_intres(coords1, coords2, resnos1, resnos2, atoms1, atoms2):
    '''Get the protein-NA interactions
    '''

    #Get CBs
    inds1 = np.argwhere(atoms1=='CB')[:,0]
    inds2 = np.argwhere(atoms2=='CB')[:,0]
    coords1, coords2 = coords1[inds1], coords2[inds2]
    resnos1, resnos2 = resnos1[inds1], resnos2[inds2]

    #Calc 2-norm - distance between chains
    mat = np.append(coords1, coords2, axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    #Get interface
    l1 = len(coords1)
    contact_dists = dists[:l1,l1:] #first dimension = ch1, second = ch2
    contacts = np.argwhere(contact_dists<8)
    #Get residue numbers
    intres1 = resnos1[contacts[:,0]]
    intres2 = resnos2[contacts[:,1]]

    return intres1, intres2

def crop(intres, resnos, croplens):
    """Crop structure by selecting the highest density of contacts
    """

    #Get the unique resnos
    u_res = np.unique(resnos)
    res_counts = np.zeros((u_res.shape[0]))
    counts = Counter(intres)
    #Assign counts
    for key in counts:
        res_counts[np.argwhere(u_res==key)[0][0]]=counts[key]

    #Go through the res_counts and crop
    selected_crops = {'contacts':[], 'cs':[], 'ce':[]}
    for croplen in croplens:
        #Add new
        selected_crops['contacts'].append(0)
        selected_crops['cs'].append(0)
        selected_crops['ce'].append(0)
        for i in range(len(res_counts)-croplen):
            #Add the crop if more contacts
            n_contacts = np.sum(res_counts[i:i+croplen])
            if n_contacts>selected_crops['contacts'][-1]:
                #Remove prev
                selected_crops['contacts'].pop()
                selected_crops['cs'].pop()
                selected_crops['ce'].pop()
                #Add new
                selected_crops['contacts'].append(n_contacts)
                selected_crops['cs'].append(i)
                selected_crops['ce'].append(i+croplen)

    #Df
    #The positions represent the unique residue positions
    crop_df = pd.DataFrame.from_dict(selected_crops)
    return crop_df


##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
meta = pd.read_csv(args.meta[0])
structure_dir = args.structure_dir[0]
minres = args.minres[0]
maxres = args.maxres[0]
outdir = args.outdir[0]

#Get all files
for ind, row in meta.iterrows():
    print(ind)
    model_coords, model_seqs, model_atoms, model_resnos = read_pdb(structure_dir+row.PDBID+'_'+row.Chain1+'-'+row.PDBID+'_'+row.Chain2+'.pdb')

    ch1, ch2 = 'A', 'B'
    intres1, intres2 = get_intres(model_coords[ch1], model_coords[ch2],
                model_resnos[ch1], model_resnos[ch2],
                model_atoms[ch1], model_atoms[ch2])

    #Crop
    #Chain 1
    crop_df1 = crop(intres1, model_resnos[ch1], np.arange(minres,maxres+1,10))
    #Chain 2
    crop_df2 = crop(intres2, model_resnos[ch2], np.arange(minres,maxres+1,10))
    #Concat
    crop_df1['Chain']=ch1
    crop_df2['Chain']=ch2
    crop_df = pd.concat([crop_df1, crop_df2])
    #Save
    crop_df.to_csv(outdir+row.PDBID+'_'+row.Chain1+'-'+row.PDBID+'_'+row.Chain2+'.csv', index=None)
