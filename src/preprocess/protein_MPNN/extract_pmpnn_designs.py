import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import Counter
import pdb

parser = argparse.ArgumentParser(description = '''Extract the ProteinMPNN designs from the fasta file.''')

parser.add_argument('--fasta_name', nargs=1, type= str, default=sys.stdin, help = 'Path to fasta with designed seqs.')
parser.add_argument('--crop_df', nargs=1, type= str, default=sys.stdin, help = 'Path to df with crops for the designed seqs.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to output csv.')

##############FUNCTIONS##############
def read_designs(fasta_name, crop_df):
    """Read the ProteinMPNN designed fasta
    """
    #Get the crop regions
    csA, csB = crop_df.cs.values
    ceA, ceB = crop_df.ce.values
    
    designs_A, designs_B = [], []
    with open(fasta_name, 'r') as file:
        for line in file:
            line = line.rstrip()
            if line[0]=='>':
                continue
            else:
                #Crop
                designs_A.append(line[csA:ceA])
                designs_B.append(line[csB:ceB])

    #Df
    design_df = pd.DataFrame()
    design_df['Chain'] = ['A', 'B']
    design_df['Length'] = 10
    design_df['designed_binder_seq'] = ['-'.join(designs_A[1:]), '-'.join(designs_B[1:])] #The first seq is the native

    return design_df

##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
fasta_name = args.fasta_name[0]
crop_df = pd.read_csv(args.crop_df[0])
outname = args.outname[0]

#Read fasta anf get the designs
design_df = read_designs(fasta_name, crop_df)
design_df.to_csv(outname)
pdb.set_trace()
