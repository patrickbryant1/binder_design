import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import Counter
import pdb

parser = argparse.ArgumentParser(description = '''Write the fasta sequences for all interacting chains.''')

parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with test set.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############
def extract_pdbs(meta, outdir):
    """Extract all interacting pairs
    """
    meta['PDB'] = [x.split('_')[0] for x in meta.CHAINID]
    counts = Counter(meta.PDB)
    keys, counts = np.array([*counts.keys()]), np.array([*counts.values()])
    sel_pdbs = keys[np.argwhere(counts>1)[:,0]]
    print('There are', sel_pdbs.shape[0], 'PDB IDS in total.')
    #Select
    sel = meta[meta['PDB'].isin(sel_pdbs)]


    #Go through sel and select all unique cluster sets
    keep_clusts = []
    keep_inds = []
    for id in sel.PDB.unique():
        sel_pdb = sel[sel.PDB==id]
        u_clusts = sel_pdb.CLUSTER.unique()
        u_clusts = '-'.join([str(x) for x in u_clusts])
        #Check if they are fetched
        if u_clusts in keep_clusts:
            continue
        else:
            keep_inds.extend([*sel_pdb.index])
            keep_clusts.append(u_clusts)

    keep = sel.loc[keep_inds]
    print('Selected',len(keep),'chains mapping to',len(keep_clusts),'unique cluster combinations')
    #count the nuber of clusters per pdb
    n_clusts_for_id = []
    for ind, row in keep.iterrows():
        sel_id = keep[keep.PDB==row.PDB]
        n_clusts_for_id.append(len(sel_id.CLUSTER.unique()))
    keep['Num_clusts_for_PDB']=n_clusts_for_id
    print('Cluster counts (>1=heteromeric):')
    print(Counter(keep.loc[keep.PDB.drop_duplicates().index]['Num_clusts_for_PDB']))

    #Save
    keep.to_csv(outdir+'protein_mpnn_test_cleaned.csv')

##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
meta = pd.read_csv(args.meta[0])
outdir = args.outdir[0]
#Extract
extract_pdbs(meta, outdir)
