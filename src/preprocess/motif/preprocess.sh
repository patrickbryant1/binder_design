MOTIF_SET=../../../data/motif/motif_meta.csv
PDBDIR=../../../data/motif/PDB/
OUTDIR=../../../data/motif/
#python3 ./parse_pdb_to_fasta.py --motif_set $MOTIF_SET \
#--pdbdir $PDBDIR --outdir $OUTDIR

#Centre of mass
for i in {2..13}
do
  ID=$(sed -n $i'p' $MOTIF_SET|cut -d ',' -f 1)
  PC=$(sed -n $i'p' $MOTIF_SET|cut -d ',' -f 3)
  RC=$(sed -n $i'p' $MOTIF_SET|cut -d ',' -f 2)
  #python3 ../calc_centre_of_mass.py --pdbdir $PDBDIR \
  #--peptide_chain $PC \
  #--receptor_chain $RC \
  #--structure_id $ID'
  echo $ID, $PC, $RC
done
