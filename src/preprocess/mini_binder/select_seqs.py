import argparse
import sys
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Select seqs for analysis.''')

parser.add_argument('--datadir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with data.')
parser.add_argument('--ids', nargs=1, type= str, default=sys.stdin, help = 'Ids to use.')
parser.add_argument('--nbelow', nargs=1, type= int, default=sys.stdin, help = 'How many below 1000 counts to use.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############

def read_seqs(filename):
    '''Read the seqs
    '''
    seq_df = {'description':[], 'Sequence':[]}

    with open(filename, 'r') as file:
        lc = 1 #Line count
        for line in file:
            line = line.rstrip()
            line = line.split(' ')
            seq_df['description'].append(line[2])
            seq_df['Sequence'].append(line[0])

    return pd.DataFrame.from_dict(seq_df)

def write_fasta(merged, outdir, id):
    '''Write fasta files for the selected seqs
    '''
    #Make dir
    try:
        os.mkdir(outdir+'fasta/'+id)
    except:
        print('Directory',outdir+'fasta/'+id,'exists.')
    merged = merged.reset_index()
    for ind, row in merged.iterrows():
        with open(outdir+'fasta/'+id+'/'+id+'_'+str(ind)+'.fasta', 'w') as file:
            file.write('>'+id+'_'+str(ind)+'\n')
            file.write(row.Sequence)

def select_from_pool(datadir, ids, nbelow, outdir):
    '''Plot the counts of the final pool
    '''

    counts = glob.glob(datadir+'*_counts.list')

    for name in counts:
        id = name.split('/')[-1].split('_')[0]
        if id not in ids:
            continue
        count_df = pd.read_csv(name, sep = ' ')
        seq_df = read_seqs(datadir+id+'_seqs.list')
        #Merge
        merged = pd.merge(count_df, seq_df, on='description', how='inner')
        #Select
        sel_below = merged[merged[merged.columns[-3]]<1000]
        sel_below = sel_below.loc[ np.random.choice(sel_below.index, min(len(sel_below),nbelow), replace=False)]
        sel_above = merged[merged[merged.columns[-3]]>=1000]
        #Merge
        merged = pd.concat([sel_below,sel_above])
        #Write fasta
        write_fasta(merged, outdir, id)
        #Save
        merged.to_csv(outdir+id+'_selected_seqs.csv', index=False)
        print('Number of total sequences for',id,'are',len(merged))
##################MAIN#######################

#Parse args
args = parser.parse_args()
datadir = args.datadir[0]
ids = args.ids[0].split(',')
nbelow = args.nbelow[0]
outdir = args.outdir[0]
#Analyse count distribution
select_from_pool(datadir, ids, nbelow, outdir)
