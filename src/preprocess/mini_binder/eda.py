import argparse
import sys
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Plot the distribution of the final pool counts.''')

parser.add_argument('--datadir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with data.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############
def plot_final_pool(datadir, outdir):
    '''Plot the counts of the final pool
    '''

    counts = glob.glob(datadir+'*_counts.list')

    for name in counts:
        df = pd.read_csv(name, sep = ' ')
        id = name.split('/')[-1].split('_')[0]

        #Plot
        fig, ax = plt.subplots(figsize=(9/2.54, 9/2.54))
        sel_below = df[df[df.columns[-2]]<1000][df.columns[-2]]
        sel_above = df[df[df.columns[-2]]>=1000][df.columns[-2]]
        plt.hist(sel_below, bins=10,label='n='+str(len(sel_below)))
        plt.hist(sel_above, bins=20,label='n='+str(len(sel_above)))
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel('NGS counts')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        plt.title(id+'|n='+str(len(df)))
        plt.tight_layout()
        plt.savefig(outdir+id+'_ngs_counts.png',dpi=300, format='png')
        plt.close()

##################MAIN#######################

#Parse args
args = parser.parse_args()
datadir = args.datadir[0]
outdir = args.outdir[0]
#Analyse count distribution
plot_final_pool(datadir, outdir)
