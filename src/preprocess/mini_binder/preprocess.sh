#Plot the distributions
DATADIR=../../../data/mini_binders/
OUTDIR=../../../data/plots/
python3 ./eda.py --datadir $DATADIR --outdir $OUTDIR

#Select sequences
OUTDIR=$DATADIR
IDS='FGFR2,TrkA,IL7Ra,VirB8'
NBELOW=1000 #How many to select below 1000 counts
# python3 ./select_seqs.py --datadir $DATADIR \
# --ids $IDS --nbelow $NBELOW --outdir $OUTDIR
