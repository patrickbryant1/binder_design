import argparse
import sys
import os
import numpy as np
import pandas as pd
import copy
import pdb

parser = argparse.ArgumentParser(description = '''Mutate a fasta seq and write the mutated sequences.''')

parser.add_argument('--fasta_path', nargs=1, type= str, default=sys.stdin, help = 'Path to fasta.')
parser.add_argument('--peptide_if_res', nargs=1, type= str, default=sys.stdin, help = 'Path to IF residues.')
parser.add_argument('--fasta_id', nargs=1, type= str, default=sys.stdin, help = 'ID.')
parser.add_argument('--number_per_step', nargs=1, type= int, default=sys.stdin, help = 'Number of mutated sequnences to create per step (10,20,...,100).')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############
def read_fasta(fasta_file):
    '''Write fasta files
    '''
    with open(fasta_file, 'r') as file:
        for line in file:
            if line[0]!='>':
                sequence = line.rstrip()
    return sequence

def mutate_seq(fastaseq, peptide_if_res, number_per_step, fasta_id, outdir):
    '''Mutate a sequence randomly and save
    '''

    restypes = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I',
                 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V' ]
    all_seqs = [fastaseq]
    seqlen = len(fastaseq)

    for npos in range(1,len(peptide_if_res)+1):
        #How many positions to mutate
        nfound=0
        while nfound < number_per_step:
            #Choose
            mut_pos = np.random.choice(peptide_if_res-1, npos,replace=False)
            #Mutate
            new_seq = copy.deepcopy(fastaseq)
            for mut_pos_i in mut_pos:
                new_aa = np.random.choice(np.setdiff1d(restypes, [fastaseq[mut_pos_i]]),1)[0]
                new_seq = new_seq[:mut_pos_i]+new_aa+new_seq[mut_pos_i+1:]
            if new_seq not in all_seqs:
                all_seqs.append(new_seq)
                nfound+=1
                #Save
                with open(outdir+fasta_id+'_'+str(npos)+'_'+str(nfound)+'.fasta', 'w') as file:
                    file.write('>'+fasta_id+'_'+str(npos)+'_'+str(nfound)+'\n')
                    file.write(new_seq)

                #print(fastaseq, new_seq)

    print(fasta_id, len(all_seqs))
##################MAIN#######################

#Parse args
args = parser.parse_args()
fastaseq = read_fasta(args.fasta_path[0])
peptide_if_res = np.load(args.peptide_if_res[0])
fasta_id = args.fasta_id[0]
number_per_step = args.number_per_step[0]
outdir = args.outdir[0]
mutate_seq(fastaseq, peptide_if_res, number_per_step, fasta_id, outdir)
