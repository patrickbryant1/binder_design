import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Merge the mutated fasta seqs into a csv.''')

parser.add_argument('--fasta_pattern', nargs=1, type= str, default=sys.stdin, help = 'Path to fasta glob pattern.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############
def read_fasta(fasta_file):
    '''Write fasta files
    '''
    with open(fasta_file, 'r') as file:
        for line in file:
            if line[0]!='>':
                sequence = line.rstrip()
    return sequence


##################MAIN#######################

#Parse args
args = parser.parse_args()

fasta_files = glob.glob(args.fasta_pattern[0]+'*/*.fasta')
outname = args.outname[0]

results_df = {'pdb_id':[], 'peptide_sequence':[]}
for name in fasta_files:
    results_df['pdb_id'].append(name.split('/')[-1].split('.fasta')[0])
    results_df['peptide_sequence'].append(read_fasta(name))

#Df
results_df = pd.DataFrame.from_dict(results_df)
results_df.to_csv(outname, index=None)
