import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb


parser = argparse.ArgumentParser(description = '''Calculate centre of mass and get the receptor CAs.''')
parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to pdbs.')
parser.add_argument('--peptide_chain', nargs=1, type= str, default=sys.stdin, help = 'Peptide chain.')
parser.add_argument('--receptor_chain', nargs=1, type= str, default=sys.stdin, help = 'Receptor chain.')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'PDB id.')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    cat_model_CA_coords = {}
    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            cat_model_CA_coords[chain.id]=[]
            #Reset res no
            res_no=0
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()

                    if atm_name=='CA':
                        cat_model_CA_coords[chain.id].append(atom.get_coord())

    for key in cat_model_CA_coords:
        cat_model_CA_coords[key] = np.array(cat_model_CA_coords[key])

    return cat_model_CA_coords



#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
CA_coords = read_pdb(args.pdbdir[0]+args.structure_id[0]+'.pdb')


#Calculate the centre of mass
# R = 1/M*sum(mi*ri); M=total mass over all atoms - mass is the same --> cancels out
#CA_mass = 12.011
peptide_coords = CA_coords[args.peptide_chain[0]]
R = np.sum(peptide_coords,axis=0)/(peptide_coords.shape[0])

#Save
np.save(args.pdbdir[0]+args.structure_id[0]+'_CM.npy',R)

#Get the receptor CAs
np.save(args.pdbdir[0]+args.structure_id[0]+'_receptor_CA.npy', CA_coords[args.receptor_chain[0]])
print(args.structure_id[0], CA_coords[args.receptor_chain[0]].shape[0])
