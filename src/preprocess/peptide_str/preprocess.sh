#Get seqs
LNRSET=../../data/LNR_set.csv
PDBDIR=../../data/PDB/
OUTDIR=../../data/
# python3 ./parse_pdb_to_fasta.py --LNR_set $LNRSET \
# --pdbdir $PDBDIR --outdir $OUTDIR

#Mutate peptide seqs
PEPTIDE_DIR=../../data/fasta/peptide/
IF_DIR=../../data/PDB/peptide/
IDS=../../data/peptide_ids_2ang.txt
# for i in {1..12}
# do
#   ID=$(sed -n $i'p' $IDS)
#   echo $ID
#   FASTAPATH=$PEPTIDE_DIR/$ID'.fasta'
#   PEPTIDE_IF=$IF_DIR/$ID'_if.npy'
#   OUTDIR=$PEPTIDE_DIR/$ID/
#   mkdir $OUTDIR
#   python3 ./mutate_fasta.py --fasta_path  $FASTAPATH \
#   --peptide_if_res $PEPTIDE_IF \
#   --fasta_id $ID --number_per_step 10 --outdir $OUTDIR
# done

#Mutated fasta to csv
# for i in {1..12}
# do
#   ID=$(sed -n $i'p' $IDS)
#   LENGTH=$(sed -n 1p $PEPTIDE_DIR/$ID'.fasta')
#   echo $ID; echo $LENGTH|cut -d '|' -f 2
#   PATTERN="$PEPTIDE_DIR/$ID"
#   OUTNAME=$PEPTIDE_DIR/$ID'_mut.csv'
#   python3 ./mutate_fasta_to_csv.py --fasta_pattern $PATTERN \
#   --outname $OUTNAME
# done

#Centre of mass
RECEPTOR_CHAINS=../../data/receptor_chains_2ang.txt
for i in {1..12}
do
  ID=$(sed -n $i'p' $IDS)
  STR_ID=$(echo $ID|cut -d '_' -f 1)
  PC=$(echo $ID|cut -d '_' -f 2)
  RC=$(sed -n $i'p' $RECEPTOR_CHAINS)
  python3 ./calc_centre_of_mass.py --pdbdir $PDBDIR \
  --peptide_chain $PC \
  --receptor_chain $RC \
  --structure_id $STR_ID
done
