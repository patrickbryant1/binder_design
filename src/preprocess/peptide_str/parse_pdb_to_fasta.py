import argparse
import sys
import os
import numpy as np
import pandas as pd
from Bio.PDB.PDBParser import PDBParser
from collections import defaultdict
import pdb

parser = argparse.ArgumentParser(description = '''Parse PDB and write individual fasta files for interacting chains.''')

parser.add_argument('--LNR_set', nargs=1, type= str, default=sys.stdin, help = 'Path to interactions.')
parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with PDB files.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############

def format_line(atm_no, atm_name, res_name, chain, res_no, x,y,z,occ,B,atm_id):
    '''Format the line into PDB
    '''

    #Get blanks
    atm_no = ' '*(5-len(atm_no))+atm_no
    atm_name = atm_name+' '*(4-len(atm_name))
    res_no = ' '*(4-len(res_no))+res_no
    x =' '*(8-len(x))+x
    y =' '*(8-len(y))+y
    z =' '*(8-len(z))+z
    occ = ' '*(6-len(occ))+occ
    B = ' '*(6-len(B))+B

    line = 'ATOM  '+atm_no+'  '+atm_name+res_name+' '+chain+res_no+' '*4+x+y+z+occ+B+' '*11+atm_id+'  '
    return line

def read_pdb(pdbname):
    '''Read PDB
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    cat_model = {} #Cat all states into 1
    cat_model_seqs = {}
    cat_model_coords = {}
    cat_model_resno = {}
    cat_model_CB_inds = {} #Get the CB inds - CA for GLY
    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            cat_model[chain.id]=[]
            cat_model_seqs[chain.id]=''
            cat_model_coords[chain.id]=[]
            cat_model_resno[chain.id]=[]
            cat_model_CB_inds[chain.id]=[]
            #Reset res no
            res_no=0
            chain_atm_ind =-1 #Make the atm no for the chain be 0 indexed
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    chain_atm_ind+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    x, y, z = format(x,'.3f'),format(y,'.3f'),format(z,'.3f')
                    occ = atom.get_occupancy()
                    occ = format(occ, '.2f')
                    B = min(100,atom.get_bfactor())
                    B = format(B, '.2f')
                    #Format line
                    line = format_line(str(atm_no), atm_name, res_name, chain.id, str(res_no),
                    x,y,z,occ,B,atom_id[0])
                    cat_model[chain.id].append(line+'\n')
                    cat_model_coords[chain.id].append(atom.get_coord())
                    cat_model_resno[chain.id].append(res_no)
                    if atm_name=='CA':
                        cat_model_seqs[chain.id]+=three_to_one[res_name]
                        if res_name=='GLY':
                            cat_model_CB_inds[chain.id].append(chain_atm_ind)
                    if atm_name=='CB':
                        cat_model_CB_inds[chain.id].append(chain_atm_ind)


    return cat_model, cat_model_seqs, cat_model_coords, cat_model_resno, cat_model_CB_inds

def write_fasta(id, seq, seqlen, outdir):
    '''Write fasta files
    '''
    with open(outdir+id+'.fasta', 'w') as file:
        file.write('>'+id+'|'+seqlen+'\n')
        file.write(seq)

def write_pdb(chain_contents, outname):
    '''Write the PDB file to outname
    '''

    with open(outname,'w') as file:
        for line in chain_contents:
            file.write(line)

def reorder_resno(resnos):
    '''Reorder the residue numbers
    '''

    reordered_resnos = []
    resno = 1
    prev_resno = resnos[0]
    for i in resnos:
        if i!=prev_resno:
            resno+=1
        reordered_resnos.append(resno)
        prev_resno=i

    return reordered_resnos

def get_interface_residues(coords1,  coords2):
    '''Extract the interface residues from the target
    '''

    #Calculate contacts
    l1 = len(coords1)
    #Calc 2-norm
    mat = np.append(coords1, coords2,axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    contact_dists = dists[l1:,:l1]
    contacts = np.argwhere(contact_dists<=8) #CB 8Å
    receptor_if_residues = np.unique(contacts[:,1])
    peptide_if_residues = np.unique(contacts[:,0])
    if peptide_if_residues.shape[0]<1:
        pdb.set_trace()
    return receptor_if_residues+1, peptide_if_residues+1 #Add one so that the residue index starts at 1

##################MAIN#######################

#Parse args
args = parser.parse_args()
#LNR set
LNR_set = pd.read_csv(args.LNR_set[0])
pdbdir = args.pdbdir[0]
outdir = args.outdir[0]

for ind, row in LNR_set.iterrows():
    pdbchains, pdbseqs, pdbcoords, pdbresno, pdbCBinds = read_pdb(pdbdir+row.pdb_id+'.pdb')
    #Write fasta
    write_fasta(row.pdb_id+'_'+row.receptor_chain,pdbseqs[row.receptor_chain],
    str(len(pdbseqs[row.receptor_chain])), outdir+'fasta/receptor/')
    write_fasta(row.pdb_id+'_'+row.peptide_chain,pdbseqs[row.peptide_chain],
    str(len(pdbseqs[row.peptide_chain])), outdir+'fasta/peptide/')
    #Write receptor PDB
    write_pdb(pdbchains[row.receptor_chain],
    outdir+'PDB/receptor/'+row.pdb_id+'_'+row.receptor_chain+'.pdb')
    #Get the interface residues
    receptor_resnos = reorder_resno(pdbresno[row.receptor_chain])
    #Check
    if max(receptor_resnos)!=len(pdbseqs[row.receptor_chain]) or min(receptor_resnos)!=1 or np.unique(receptor_resnos).shape[0]!=len(pdbseqs[row.receptor_chain]):
        pdb.set_trace()

    receptor_if_residues, peptide_if_residues = get_interface_residues(np.array(pdbcoords[row.receptor_chain])[pdbCBinds[row.receptor_chain]],
                        np.array(pdbcoords[row.peptide_chain])[pdbCBinds[row.peptide_chain]])

    #Save receptor if residues
    np.save(outdir+'PDB/receptor/'+row.pdb_id+'_'+row.receptor_chain+'_if.npy', receptor_if_residues)
    #Save peptide if residues
    np.save(outdir+'PDB/peptide/'+row.pdb_id+'_'+row.peptide_chain+'_if.npy', peptide_if_residues)
