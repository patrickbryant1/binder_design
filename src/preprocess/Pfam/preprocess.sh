
#Write all pairs to evaluate
META=../../../data/Pfam/meta.csv
PDBDIR=../../../data/Pfam/PDB/
OUTDIR=../../../data/Pfam/PDB/pairs/
#python3 ./write_pairs.py --meta $META \
#--structure_dir $PDBDIR --outdir $OUTDIR

#Select interface crops to predict
MINRES=10 #Min interface segment
MAXRES=50 #Max
STEP=10 #Step btw min and max sections
STRDIR=../../../data/Pfam/PDB/pairs/
OUTDIR=../../../data/Pfam/crops/
# python3 ./select_crop.py --meta $META \
# --structure_dir $STRDIR \
# --minres $MINRES --maxres $MAXRES \
# --outdir $OUTDIR

OUTDIR=../../../data/Pfam/fasta/
python3 ./write_fasta.py --meta $META \
--structure_dir $STRDIR --outdir $OUTDIR
