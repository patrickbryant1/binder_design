import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB import MMCIFParser
import pdb

parser = argparse.ArgumentParser(description = '''Parse PDB files and get interacting chains.''')

parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with interacting chains.')
parser.add_argument('--structure_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to PDB files.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    model_coords = {}
    model_seqs = {}
    model_atoms = {}
    model_resnos = {}

    for model in struc:
        for chain in model:
            #Save
            model_coords[chain.id]=[]
            model_seqs[chain.id]=[]
            model_atoms[chain.id]=[]
            model_resnos[chain.id]=[]

            #Save residue
            for residue in chain:
                res_name = residue.get_resname()
                if res_name not in three_to_one.keys():
                    continue
                for atom in residue:
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    #Save
                    model_coords[chain.id].append(atom.get_coord())
                    model_seqs[chain.id].append(res_name)
                    model_atoms[chain.id].append(atom_id)
                    model_resnos[chain.id].append(residue.get_id()[1])



    #Convert to array
    for key in model_coords:
        model_coords[key] = np.array(model_coords[key])
        model_seqs[key] = np.array(model_seqs[key])
        model_atoms[key] = np.array(model_atoms[key])
        model_resnos[key] = np.array(model_resnos[key])

    return model_coords, model_seqs, model_atoms, model_resnos

def format_line(atm_no, atm_name, res_name, chain, res_no, coord, occ, B , atm_id):
    '''Format the line into PDB
    '''

    #Get blanks
    atm_no = ' '*(5-len(atm_no))+atm_no
    atm_name = atm_name+' '*(4-len(atm_name))
    res_name = ' '*(3-len(res_name))+res_name
    res_no = ' '*(4-len(res_no))+res_no
    x,y,z = coord
    x,y,z = str(np.round(x,3)), str(np.round(y,3)), str(np.round(z,3))
    x =' '*(8-len(x))+x
    y =' '*(8-len(y))+y
    z =' '*(8-len(z))+z
    occ = ' '*(6-len(occ))+occ
    B = ' '*(6-len(B))+B

    line = 'ATOM  '+atm_no+'  '+atm_name+res_name+' '+chain+res_no+' '*4+x+y+z+occ+B+' '*11+atm_id+'  '
    return line


def write_pdb(coords1, coords2, resnos1, resnos2, atoms1, atoms2, seq1, seq2, outname):
    """Write the interacting pair
    """


    with open(outname, 'w') as file:
        atmno = 1
        #Write chain 1
        chain='A'
        for i in range(len(coords1)):
            file.write(format_line(str(atmno), atoms1[i], seq1[i],
            chain, str(resnos1[i]), coords1[i], '1.00','100',atoms1[i][0])+'\n')
            atmno+=1

        #Write chain 2
        chain='B'
        for i in range(len(coords2)):
            file.write(format_line(str(atmno), atoms2[i], seq2[i],
            chain, str(resnos2[i]),coords2[i], '1.00','100',atoms2[i][0])+'\n')
            atmno+=1

##################MAIN#######################

#Parse args
args = parser.parse_args()
#Data
meta = pd.read_csv(args.meta[0])
structure_dir = args.structure_dir[0]
outdir = args.outdir[0]

#Get all files
for ind, row in meta.iterrows():

    model_coords, model_seqs, model_atoms, model_resnos = read_pdb(structure_dir+row.PDB+'.pdb')

    ch1, ch2 = row['Chain 1'], row['Chain 2']

    #Write pdb
    write_pdb(model_coords[ch1], model_coords[ch2],
                model_resnos[ch1], model_resnos[ch2],
                model_atoms[ch1], model_atoms[ch2],
                model_seqs[ch1], model_seqs[ch2],
                outdir+row.id1+'-'+row.id2+'.pdb')
