#Get seqs and merge to a df and write fasta
PRMFAMS=../../../data/PRM/prm_families.csv
RECEPTOR_SEQS=../../../data/PRM/receptor_seqs.tsv
OUTDIR=../../../data/PRM/
python3 ./merge_dfs_write_fasta.py --prm_families $PRMFAMS \
--receptor_seqs $RECEPTOR_SEQS --outdir $OUTDIR
