import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb

parser = argparse.ArgumentParser(description = '''Merge csvs and write individual fasta files for the receptors.''')

parser.add_argument('--prm_families', nargs=1, type= str, default=sys.stdin, help = 'Path to families.')
parser.add_argument('--receptor_seqs', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with receptor seqs.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to output directory. Include /in end')

##############FUNCTIONS##############

def merge_dfs(prm_families, receptor_seqs, outdir):
    '''Merge the peptide and receptor seqs into one df
    '''

    merged = {'receptor_id':[], 'receptor_family':[],'peptide_seq':[],
            'ELISA_ratio':[], 'receptor_seqlen':[]}
    for ind, row in prm_families.iterrows():
        peptide_seqs = pd.read_csv(outdir+row.LINK.split('/')[-1],sep='\t')
        merged['receptor_id'].extend([row.UNIPROT_ID]*len(peptide_seqs))
        merged['receptor_family'].extend([row.FAMILY]*len(peptide_seqs))
        merged['peptide_seq'].extend([*peptide_seqs.PeptideSequence.values])
        merged['ELISA_ratio'].extend([*peptide_seqs['ELISA ratio'].values])
        merged['receptor_seqlen'].extend([len(receptor_seqs[receptor_seqs.Entry==row.UNIPROT_ID]['Sequence'].values[0])]*len(peptide_seqs))
    #Create df
    merged = pd.DataFrame.from_dict(merged)
    #Save
    merged.to_csv(outdir+'prm_set_all.csv', index=None)

def write_fasta(receptor_seqs, outdir):
    '''Write fasta files for the receptor sequences
    '''

    for ind, row in receptor_seqs.iterrows():
        with open(outdir+'fasta/'+row.Entry+'.fasta', 'w') as file:
            file.write('>'+row.Entry+'|'+str(len(row.Sequence))+'\n')
            file.write(row.Sequence)

##################MAIN#######################

#Parse args
args = parser.parse_args()
#LNR set
prm_families = pd.read_csv(args.prm_families[0])
receptor_seqs = pd.read_csv(args.receptor_seqs[0], sep='\t')
outdir = args.outdir[0]

merge_dfs(prm_families, receptor_seqs, outdir)
write_fasta(receptor_seqs, outdir)
