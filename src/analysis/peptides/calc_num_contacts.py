import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from Bio.PDB.PDBParser import PDBParser
import pdb


parser = argparse.ArgumentParser(description = '''Calculate the number of contacts''')
parser.add_argument('--pdbfile', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb file.')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write the number of contacts')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''

    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    model_coords = {}
    model_plddt = {}

    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            model_coords[chain.id]=[]
            model_plddt[chain.id]=[]
            for residue in chain:
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    if atm_name=='CB':
                        model_coords[chain.id].append([x,y,z])
                        model_plddt[chain.id].append(atom.bfactor)



    #Create np arrays
    for chain in model_coords:
        model_coords[chain] = np.array(model_coords[chain])
        model_plddt[chain] = np.array(model_plddt[chain])

    return model_coords, model_plddt

def get_num_contacts(chain_coords, chain_plddt):
    '''Calculate the number of contacts between the chains
    '''

    #Calc 2-norm
    receptor_coords, peptide_coords = chain_coords['A'], chain_coords['B']
    receptor_plddt, peptide_plddt = chain_plddt['A'], chain_plddt['B']
    l1 = len(receptor_coords)
    mat = np.append(receptor_coords, peptide_coords,axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    contact_dists = dists[:l1,l1:] #rows, columns
    contacts = np.argwhere(contact_dists<8)

    #Get the interface plDDT
    if_plddt = np.concatenate([receptor_plddt[contacts[:,0]], peptide_plddt[contacts[:,1]]])

    return if_plddt.mean(), contacts.shape[0]


#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
chain_coords, chain_plddt = read_pdb(args.pdbfile[0]) #Read the CB coords
structure_id = args.structure_id[0]
outdir = args.outdir[0]
if_plddt, num_contacts = get_num_contacts(chain_coords, chain_plddt)
df = pd.DataFrame()
df['ID']=[structure_id]
df['avg_if_plDDT']=[if_plddt]
df['num_contacts']=[num_contacts]
df.to_csv(outdir+structure_id+'_num_contacts.csv', index=None)
