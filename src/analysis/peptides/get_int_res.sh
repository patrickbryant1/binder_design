# IDS=../../data/peptide_ids_2ang.txt
# NATIVE_PDBDIR=../../data/PDB
# DESIGN_PDBDIR=../../data/binder_opt/PDB
# RECEPTOR_CHAINS=../../data/receptor_chains_2ang.txt
#
# for i in {1..12}
# do
#   ID=$(sed -n $i'p' $IDS)
#   ID=$(echo $ID|cut -d '_' -f 1)
#   STRUCTURE=$NATIVE_PDBDIR/$ID'.pdb'
#   RECEPTOR_CHAIN=$(sed -n $i'p' $RECEPTOR_CHAINS)
#   PEPCHAIN=$(sed -n $i'p' $IDS|cut -d '_' -f 2)
#   echo $ID
#   python3 ./get_interacting_residues.py --structure $STRUCTURE --structure_id $ID \
#   --receptor_chain $RECEPTOR_CHAIN \
#  --peptide_chain $PEPCHAIN \
#   --outdir $NATIVE_PDBDIR/
#
# done


IDS=../../data/motif/ids.txt
NATIVE_PDBDIR=../../data/motif/PDB
RECEPTOR_CHAINS=../../data/motif/receptor_chains.txt
PEPTIDE_CHAINS=../../data/motif/peptide_chains.txt

for i in {1..12}
do
  ID=$(sed -n $i'p' $IDS)
  ID=$(echo $ID|cut -d '_' -f 1)
  STRUCTURE=$NATIVE_PDBDIR/$ID'.pdb'
  RECEPTOR_CHAIN=$(sed -n $i'p' $RECEPTOR_CHAINS)
  PEPCHAIN=$(sed -n $i'p' $PEPTIDE_CHAINS)
  echo $ID
  python3 ./get_interacting_residues.py --structure $STRUCTURE --structure_id $ID \
  --receptor_chain $RECEPTOR_CHAIN \
 --peptide_chain $PEPCHAIN \
  --outdir $NATIVE_PDBDIR/

done
