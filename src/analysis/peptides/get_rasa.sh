PDBDIR=/proj/berzelius-2021-29/users/x_patbr/results/binder/design_opt/
STRIDS=../../data/peptide_ids_2ang.txt
DSSP=/proj/berzelius-2021-29/users/x_patbr/dssp
OUTDIR=../../data/binder_opt/init1/
NINIT=3

for LN in {1..12}
do
	STRID=$(sed -n $LN'p' $STRIDS|cut -d '_' -f 1)
	echo $STRID
	IFRES=../../data/PDB/receptor/$STRID*_if.npy
	python3 ./calc_RASA.py --pdbdir $PDBDIR/$STRID'_'$NINIT/mc_search/ --structure_id $STRID \
	--DSSP $DSSP --if_residues $IFRES --outdir $OUTDIR
done
