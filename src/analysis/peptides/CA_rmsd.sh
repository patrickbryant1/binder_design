IDS=../../data/peptide_ids_2ang.txt
RECEPTOR_CHAINS=../../data/receptor_chains_2ang.txt
NATIVEDIR=../../data/PDB/
PREDDIR=../../data/binder_opt/PDB/
NINIT=3

for i in {1..12}
do
  STRID=$(sed -n $i'p' $IDS|cut -d '_' -f 1)
  RECEPTOR_CHAIN=$(sed -n $i'p' $RECEPTOR_CHAINS)
  PEPCHAIN=$(sed -n $i'p' $IDS|cut -d '_' -f 2)
  NATIVE=$NATIVEDIR/$STRID'.pdb'
  PRED=$PREDDIR/$STRID'_'$NINIT'_best_complex.pdb'
  OUTDIR=../../data/binder_opt/init$NINIT/
  python3 ./calc_peptide_CA_rmsd.py --native_structure $NATIVE \
  --prediction $PRED --structure_id $STRID --receptor_chain $RECEPTOR_CHAIN \
  --peptide_chain $PEPCHAIN --outdir $OUTDIR
done
