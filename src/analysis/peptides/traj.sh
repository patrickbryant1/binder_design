LOSSES=/home/patrick/Desktop/figs/binder/3c3o/loss.npy
IFRES=/home/patrick/binder_design/data/PDB/receptor/3c3o_A_if.npy
PDBDIR=/home/patrick/Desktop/figs/binder/3c3o/
OUTDIR=/home/patrick/Desktop/figs/binder/3c3o/
python3 ./vis_trajectory.py --losses $LOSSES \
--if_res $IFRES --pdbdir $PDBDIR --outdir $OUTDIR
