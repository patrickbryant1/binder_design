import argparse
import sys
import os
import numpy as np
import pandas as pd
import torch
import esm
import glob

import pdb


parser = argparse.ArgumentParser(description = '''Get the ESM-1b LM probabilities for a sequence.''')
parser.add_argument('--sequence_df', nargs=1, type= str, default=sys.stdin, help = 'Sequences to be evaluated.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Where to write the output')

##############FUNCTIONS##############
def load_esm():
    '''
    Load ESM-1b model
    From: https://github.com/facebookresearch/esm
    model, alphabet = torch.hub.load("facebookresearch/esm:main", "esm1b_t33_650M_UR50S")
    '''
    esm_model, esm_alphabet = esm.pretrained.esm1b_t33_650M_UR50S()
    esm_batch_converter = esm_alphabet.get_batch_converter()
    esm_model.eval()  # disables dropout for deterministic results
    return esm_model, esm_alphabet, esm_batch_converter

def get_esm_probabilities(sequences, esm_model, esm_alphabet, esm_batch_converter):
    '''Get the predicted probabilities from ESM-1b for a sequence
    '''

    tokens = esm_alphabet.tok_to_idx
    data = []
    for i in range(len(sequences)):
        data.append(("protein"+str(i), sequences[i]))
    batch_labels, batch_strs, batch_tokens = esm_batch_converter(data)
    # Extract per-residue representations (on CPU)
    with torch.no_grad():
        results = esm_model(batch_tokens, repr_layers=[33], return_contacts=True)

    #Select the logits for the sequence: not start,end tokens
    logits = results["logits"][:,1:-1,:]
    lsoftmax = torch.nn.LogSoftmax(dim=1)
    #Map sequence prob from token
    sequence_aa_probs = []
    for i in range(len(sequences)):
        sequence = sequences[i]
        logits_i = lsoftmax(logits[i])
        aa_probs = []
        for j in range(len(sequence)):
            aa_probs.append(logits_i[j,tokens[sequence[j]]])
        sequence_aa_probs.append(aa_probs)

    return [np.exp(x) for x in sequence_aa_probs]


#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
sequence_df = pd.read_csv(args.sequence_df[0])
outname = args.outname[0]
#Load the ESM-1b model - this will be used for the directed evolution
esm_model, esm_alphabet, esm_batch_converter = load_esm()
sequence_aa_probs = get_esm_probabilities([*sequence_df.peptide_sequence.values], esm_model, esm_alphabet, esm_batch_converter)

df = pd.DataFrame()
df['ID']=sequence_df.pdb_id.values
df['esm_prob']=[x.mean() for x in sequence_aa_probs]
df.to_csv(outname, index=None)
