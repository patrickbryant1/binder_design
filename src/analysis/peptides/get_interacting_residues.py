import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb


parser = argparse.ArgumentParser(description = '''Get the interacting residues between the receptor chain and the peptide.''')
parser.add_argument('--structure', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb with native structure.')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--receptor_chain', nargs=1, type= str, default=sys.stdin, help = 'Receptor chain in native structure')
parser.add_argument('--peptide_chain', nargs=1, type= str, default=sys.stdin, help = 'Peptide chain in native structure')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    cat_model_seqs = {}
    cat_model_CB_coords = {}

    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            cat_model_seqs[chain.id]=''
            cat_model_CB_coords[chain.id]=[]

            #Reset res no
            res_no=0
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    if atm_name=='CB':
                        cat_model_seqs[chain.id]+=three_to_one[res_name]
                        cat_model_CB_coords[chain.id].append(atom.get_coord())



    #Create np arrays
    for key in cat_model_seqs:
        cat_model_seqs[key] = cat_model_seqs[key]
        cat_model_CB_coords[key] = np.array(cat_model_CB_coords[key])

    return cat_model_seqs, cat_model_CB_coords


def get_interacting_residues(seqs, CB_coords, receptor_chain, peptide_chain):
    '''Get the interacting residues
    '''
    #Get receptor - longest chain

    #Get coords and seqs
    receptor_coords, receptor_seq = CB_coords[receptor_chain], seqs[receptor_chain]
    peptide_coords, peptide_seq = CB_coords[peptide_chain], seqs[peptide_chain]

    #Calc 2-norm
    l1 = len(receptor_seq)
    mat = np.append(receptor_coords,peptide_coords, axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    contact_dists = dists[:l1,l1:] #first dimension = receptor, second = peptide
    contacts = np.argwhere(contact_dists<8)

    #Create a df
    int_res_df = {'Receptor_resno':np.unique(contacts[:,0]), 'Peptide_resno':[],
                  'Receptor_res':[], 'Peptide_res':[]}
    for rr in np.unique(contacts[:,0]):
        int_res_df['Peptide_resno'].append('-'.join([str(x) for x in contacts[:,1][np.argwhere(contacts[:,0]==rr)][:,0]]))
        int_res_df['Peptide_res'].append(''.join([peptide_seq[x] for x in contacts[:,1][np.argwhere(contacts[:,0]==rr)][:,0]]))
        int_res_df['Receptor_res'].append(receptor_seq[rr])

    int_res_df = pd.DataFrame.from_dict(int_res_df)

    return int_res_df

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
seqs, CB_coords = read_pdb(args.structure[0])
structure_id = args.structure_id[0]
receptor_chain = args.receptor_chain[0]
peptide_chain = args.peptide_chain[0]
outdir = args.outdir[0]

int_res_df = get_interacting_residues(seqs, CB_coords, receptor_chain, peptide_chain)

#Save
int_res_df.to_csv(outdir+structure_id+'_interacting_residues.csv', index=None)
