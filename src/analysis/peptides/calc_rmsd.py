import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb


parser = argparse.ArgumentParser(description = '''Superimpose on the receptor chain and calculate the RMSD to the binder.''')
parser.add_argument('--native_structure', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb with native structure.')
parser.add_argument('--prediction', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb with prediction')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--receptor_chain', nargs=1, type= str, default=sys.stdin, help = 'Receptor chain in native structure')
parser.add_argument('--peptide_chain', nargs=1, type= str, default=sys.stdin, help = 'Peptide chain in native structure')
parser.add_argument('--peptide_ifres', nargs=1, type= str, default=sys.stdin, help = 'Peptide chain interface residues')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    cat_model_seqs = {}
    cat_model_coords = {}
    cat_model_resno = {}
    cat_model_atm_type = {}
    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            cat_model_seqs[chain.id]=''
            cat_model_coords[chain.id]=[]
            cat_model_resno[chain.id]=[]
            cat_model_atm_type[chain.id]=[]
            #Reset res no
            res_no=0
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    cat_model_coords[chain.id].append(atom.get_coord())
                    cat_model_resno[chain.id].append(res_no)
                    cat_model_atm_type[chain.id].append(atm_name)
                    if atm_name=='CA':
                        cat_model_seqs[chain.id]+=three_to_one[res_name]



    #Create np arrays
    for key in cat_model_seqs:
        cat_model_seqs[key] = np.array(cat_model_seqs[key])
        cat_model_coords[key] = np.array(cat_model_coords[key])
        cat_model_resno[key] = np.array(cat_model_resno[key])
        cat_model_atm_type[key] = np.array(cat_model_atm_type[key])

    return cat_model_seqs, cat_model_coords, cat_model_resno, cat_model_atm_type


def calc_rmsd(native_coords, native_resnos, native_atm_types,
            pred_coords, pred_resnos, pred_atm_types,
            receptor_chain, peptide_chain, peptide_ifres, outdir):
    '''Supepose the native receptor with the predicted and calculate the RMSD to the
    peptide binder
    '''

    #Set the coordinates to be superimposed.
    #coords will be put on top of reference_coords.
    native_receptor_coords, pred_receptor_coords = native_coords[receptor_chain], pred_coords['A']
    #Get CA inds
    native_CA_inds, pred_CA_inds = np.argwhere(native_atm_types[receptor_chain]=='CA')[:,0], np.argwhere(pred_atm_types['A']=='CA')[:,0]
    sup = SVDSuperimposer()
    sup.set(native_receptor_coords[native_CA_inds], pred_receptor_coords[pred_CA_inds]) #(reference_coords, coords)
    sup.run()
    rot, tran = sup.get_rotran()
    #Rotate coords from new chain to its new relative position/orientation
    rotated_coords = np.dot(pred_coords['B'], rot) + tran
    #Calculate rmsd
    #Calc 2-norm
    l1 = len(rotated_coords)
    mat = np.append(rotated_coords,native_coords[peptide_chain],axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    contact_dists = dists[:l1,l1:]
    atom_atom_rmsd = np.diagonal(contact_dists)
    #Get atoms belonging to if res
    if_res_atom_pos = []
    for if_res in peptide_ifres:
        if_res_atom_pos.extend([*np.argwhere(pred_resnos['B']==if_res)[:,0]])
    #The first dimension is the pred coords
    #Sometimes not all atoms are in the peptide pdb file
    if_res_atom_pos = np.array(if_res_atom_pos)
    if_res_atom_pos = if_res_atom_pos[np.argwhere(if_res_atom_pos<len(atom_atom_rmsd))][:,0]
    peptide_if_rmsd = atom_atom_rmsd[if_res_atom_pos].mean()

    return peptide_if_rmsd

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
native_seqs, native_coords, native_resnos, native_atm_types = read_pdb(args.native_structure[0]) #cat_model_seqs, cat_model_coords, cat_model_resno
pred_seqs, pred_coords, pred_resnos, pred_atm_types = read_pdb(args.prediction[0])
structure_id = args.structure_id[0]
receptor_chain = args.receptor_chain[0]
peptide_chain = args.peptide_chain[0]
peptide_ifres = np.load(args.peptide_ifres[0])
outdir = args.outdir[0]

average_rmsd = calc_rmsd(native_coords, native_resnos, native_atm_types,
                        pred_coords, pred_resnos, pred_atm_types,
                        receptor_chain, peptide_chain, peptide_ifres, outdir)
#Save
df = pd.DataFrame()
df['ID']=[structure_id]
df['RMSD']=[average_rmsd]
df.to_csv(outdir+structure_id+'.csv', index=None)
