import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb


parser = argparse.ArgumentParser(description = '''Superimpose on the receptor chain and calculate the RMSD to the binder using CAs.''')
parser.add_argument('--native_structure', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb with native structure.')
parser.add_argument('--prediction', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb with prediction')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--receptor_chain', nargs=1, type= str, default=sys.stdin, help = 'Receptor chain in native structure')
parser.add_argument('--peptide_chain', nargs=1, type= str, default=sys.stdin, help = 'Peptide chain in native structure')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    cat_model_coords = {}
    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            cat_model_coords[chain.id]=[]
            #Reset res no
            res_no=0
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()

                    if atm_name=='CA':
                        cat_model_coords[chain.id].append(atom.get_coord())



    #Create np arrays
    for key in cat_model_coords:
        cat_model_coords[key] = np.array(cat_model_coords[key])

    return cat_model_coords


def calc_rmsd(native_coords, pred_coords, receptor_chain, peptide_chain, outdir):
    '''Supepose the native receptor with the predicted and calculate the RMSD to the
    peptide binder
    '''

    #Set the coordinates to be superimposed.
    #coords will be put on top of reference_coords.
    native_receptor_coords, pred_receptor_coords = native_coords[receptor_chain], pred_coords['A']
    #Get CA inds
    sup = SVDSuperimposer()
    sup.set(native_receptor_coords, pred_receptor_coords) #(reference_coords, coords)
    sup.run()
    rot, tran = sup.get_rotran()
    #Rotate coords from new chain to its new relative position/orientation
    rotated_coords = np.dot(pred_coords['B'], rot) + tran
    #Calculate rmsd
    #Calc 2-norm
    l1 = len(rotated_coords)
    mat = np.append(rotated_coords,native_coords[peptide_chain],axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    contact_dists = dists[:l1,l1:]
    CA_rmsd = np.diagonal(contact_dists)

    return CA_rmsd.mean()

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
native_coords = read_pdb(args.native_structure[0]) #Read CA coords
pred_coords = read_pdb(args.prediction[0])
structure_id = args.structure_id[0]
receptor_chain = args.receptor_chain[0]
peptide_chain = args.peptide_chain[0]
outdir = args.outdir[0]

average_rmsd = calc_rmsd(native_coords, pred_coords, receptor_chain, peptide_chain, outdir)
#Save
df = pd.DataFrame()
df['ID']=[structure_id]
df['CA_RMSD']=[average_rmsd]
df.to_csv(outdir+structure_id+'_CA_rmsd.csv', index=None)
print(df)
