LNR_SET=../../../data/peptide/LNR_set.csv
NATIVE_LM_PROBS=../../../data/peptide/native_lm_probs.csv
NATIVE_PEPTIDE_IF_RMSD=../../../data/peptide/native_peptide_if_rmsd.csv #RMSD to native peptide IF residues
NATIVE_PEPTIDE_PLDDT8=../../../data/peptide/native_peptide_plddt_8.csv
NATIVE_PEPTIDE_IF_DISTS8=../../../data/peptide/native_peptide_if_dists_8.csv
MUTATED_PEPTIDE_PLDDT8=../../../data/peptide/mutated_peptide_plddt_8.csv
MUTATED_PEPTIDE_IF_DISTS8=../../../data/peptide/mutated_peptide_if_dists_8.csv
MUTATED_PEPTIDE_SEQS_BLOSUM62_SCORES=../../../data/peptide/mut_peptide_seq_blosum62.csv
BINDER_OPT_DIR=../../data/binder_opt/init3/
NATIVE_PEPTIDE_FASTADIR=../../../data/peptide/fasta/peptide/
NATIVE_PDB_DIR=../../data/peptide/PDB/
PRM_META=../../../data/PRM/prm_set_all.csv
PRM_PLDDT=../../../data/PRM/all_plddt.csv
PRM_NCONTACTS=../../../data/PRM/all_num_contacts.csv
PLDDT_ONLY_OPTDIR=../../data/binder_opt/plDDT_only/
CB_RECEPTOR_RMSD=../../data/binder_opt/init3/all_CB_receptor_rmsd.csv
MOTIF_DIR=../../../data/motif/
MOTIF_META=../../../data/motif/motif_meta.csv
MINIBIND_DIR=../../../data/mini_binders/
OUTDIR=../../data/plots/

python3 ./vis.py --LNR_set $LNR_SET \
--native_lm_probs $NATIVE_LM_PROBS \
--native_peptide_if_rmsd $NATIVE_PEPTIDE_IF_RMSD \
--native_peptide_plddt8 $NATIVE_PEPTIDE_PLDDT8 \
--native_peptide_if_dists8 $NATIVE_PEPTIDE_IF_DISTS8 \
--mutated_peptide_plddt8 $MUTATED_PEPTIDE_PLDDT8 \
--mutated_peptide_if_dists8 $MUTATED_PEPTIDE_IF_DISTS8 \
--mut_peptide_seq_blosum62 $MUTATED_PEPTIDE_SEQS_BLOSUM62_SCORES \
--binder_opt_dir $BINDER_OPT_DIR \
--native_peptide_fastadir $NATIVE_PEPTIDE_FASTADIR \
--native_pdbdir $NATIVE_PDB_DIR \
--prm_meta $PRM_META \
--prm_plddt $PRM_PLDDT \
--prm_num_contacts $PRM_NCONTACTS \
--plDDT_only_optdir $PLDDT_ONLY_OPTDIR \
--all_CB_receptor_rmsd $CB_RECEPTOR_RMSD \
--motif_dir $MOTIF_DIR \
--motif_meta $MOTIF_META \
--mini_binder_dir $MINIBIND_DIR \
--outdir $OUTDIR
