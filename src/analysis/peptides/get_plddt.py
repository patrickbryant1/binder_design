import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.SVDSuperimposer import SVDSuperimposer
import pdb


parser = argparse.ArgumentParser(description = '''Get plDDT of the peptide.''')
parser.add_argument('--prediction', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb with prediction')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    cat_model_seqs = {}
    cat_model_plddt = {}

    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            cat_model_seqs[chain.id]=''
            cat_model_plddt[chain.id]=[]

            #Reset res no
            res_no=0
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()

                    if atm_name=='CA':
                        cat_model_seqs[chain.id]+=three_to_one[res_name]
                        cat_model_plddt[chain.id].append(atom.bfactor)

    #Create np arrays
    for key in cat_model_seqs:
        cat_model_seqs[key] = np.array(cat_model_seqs[key])
        cat_model_plddt[key] = np.array(cat_model_plddt[key])

    return cat_model_seqs, cat_model_plddt


#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
pred_seqs, pred_plddt = read_pdb(args.prediction[0])
structure_id = args.structure_id[0]
outdir = args.outdir[0]

#Save
df = pd.DataFrame()
df['ID']=[structure_id]
df['peptide_plDDT']=[pred_plddt['B'].mean()]
df.to_csv(outdir+structure_id+'_plddt.csv', index=None)
