
import sys
import os
import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import argparse
import glob
from sklearn import metrics
from scipy.signal import savgol_filter
from scipy.stats import spearmanr
import logomaker


parser = argparse.ArgumentParser(description = '''Visualise''')
parser.add_argument('--LNR_set', nargs=1, type= str, required=True, help = "LNR set")
parser.add_argument('--native_lm_probs', nargs=1, type= str, required=True, help = "native_lm_probs")
parser.add_argument('--native_peptide_if_rmsd', nargs=1, type= str, required=True, help = "RMSD to if res")
parser.add_argument('--native_peptide_plddt8', nargs=1, type= str, required=True, help = "peptide plDDT of 8 recycles")
parser.add_argument('--native_peptide_if_dists8', nargs=1, type= str, required=True, help = "IF dists using 8 recycles")
parser.add_argument('--mutated_peptide_plddt8', nargs=1, type= str, required=True, help = "peptide plDDT of 8 recycles for the mutated peptides")
parser.add_argument('--mutated_peptide_if_dists8', nargs=1, type= str, required=True, help = "IF dists using 8 recycles for the mutated peptides")
parser.add_argument('--mut_peptide_seq_blosum62', nargs=1, type= str, required=True, help = "Seqs and BLOSUM62 scores for the mutated peptides")
parser.add_argument('--binder_opt_dir', nargs=1, type= str, required=True, help = "Results from the binder opt.")
parser.add_argument('--native_peptide_fastadir', nargs=1, type= str, required=True, help = "Dir with fasta for native seqs.")
parser.add_argument('--native_pdbdir', nargs=1, type= str, required=True, help = "Dir with native PDBs.")
parser.add_argument('--prm_meta', nargs=1, type= str, required=True, help = "Meta with all affinity values for the PRM set.")
parser.add_argument('--prm_plddt', nargs=1, type= str, required=True, help = "Average peptide plDDT values for the PRM set.")
parser.add_argument('--prm_num_contacts', nargs=1, type= str, required=True, help = "Number of contacts between receptor and peptide for the PRM set.")
parser.add_argument('--plDDT_only_optdir', nargs=1, type= str, required=True, help = "plDDT only opt.")
parser.add_argument('--all_CB_receptor_rmsd', nargs=1, type= str, required=True, help = "CB receptor RMSD.")
parser.add_argument('--motif_dir', nargs=1, type= str, help = 'Directory with motif search results.')
parser.add_argument('--motif_meta', nargs=1, type= str, help = 'CSV with motif meta.')
parser.add_argument('--mini_binder_dir', nargs=1, type= str, help = 'Path to mini binder dir.')
parser.add_argument('--outdir', nargs=1, type= str, help = 'Outdir.')


#################FUNCTIONS#################
def native_rmsd_num_recycles(native_peptide_if_rmsd, outdir):
    '''Analyse the relationship between the number of iterations and the accuracy
    '''

    native_peptide_if_rmsd['Recycles']=[int(x.split('_')[1]) for x in native_peptide_if_rmsd.ID.values]
    native_peptide_if_rmsd['ID']=[x.split('_')[0] for x in native_peptide_if_rmsd.ID.values]
    medians = native_peptide_if_rmsd.groupby('Recycles').median()['RMSD'].values

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.swarmplot(x='Recycles',y='RMSD',data=native_peptide_if_rmsd,size=2)
    #plt.yscale('log')
    plt.ylim([0,20])
    plt.scatter(np.arange(10),medians,color='k',marker='x')
    plt.title('Recycles and RMSD')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'recycles_rmsd.png',dpi=300, format='png')
    plt.close()

    #Analyse the Cumulative fraction
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    for rec in np.arange(1,11):
        sel = native_peptide_if_rmsd[native_peptide_if_rmsd.Recycles==rec]
        step=0.2
        fracs = []
        for t in np.arange(0,5.5,step):
            sel_t = sel[(sel.RMSD>t)&(sel.RMSD<=t+step)]
            fracs.append(len(sel_t))
            if t+step==2:
                cf = np.cumsum(fracs)
                print(rec, cf[-1], cf[-1]/len(sel))
        plt.plot(np.arange(0,5.5,step),100*np.cumsum(fracs)/len(sel),label=rec,linewidth=1)

    plt.legend()
    plt.title('Recycles and cumulative fraction')
    plt.xlabel('RMSD')
    plt.ylabel('Fraction (%)')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.axvline(2,0,1,color='grey', linestyle='--', linewidth=1)
    plt.tight_layout()
    plt.savefig(outdir+'recycles_rmsd_cum_frac.png',dpi=300, format='png')
    plt.close()



def distinguish_native(native_peptide_if_rmsd, native_peptide_if_dists8 , native_lm_probs, native_peptide_plddt8, outdir):
    '''Analyse the possibility to distinguish between accurately predicted native peptides
    '''
    #Select 8
    native_peptide_if_rmsd['Recycles']=[int(x.split('_')[1]) for x in native_peptide_if_rmsd.ID.values]
    native_peptide_if_rmsd['ID']=[x.split('_')[0] for x in native_peptide_if_rmsd.ID.values]
    native_peptide_if_rmsd8 = native_peptide_if_rmsd[native_peptide_if_rmsd.Recycles==8]

    #Merge
    native_peptide_if_dists8['ID']=[x.split('_')[0] for x in native_peptide_if_dists8.ID]
    native_peptide_plddt8['ID'] = [x.split('_')[0] for x in native_peptide_plddt8.ID]
    merged = pd.merge(native_peptide_if_rmsd8, native_lm_probs, on='ID')
    merged = pd.merge(merged, native_peptide_plddt8, on='ID')
    merged = pd.merge(merged, native_peptide_if_dists8,on='ID')

    #ROC
    labels = np.zeros(len(merged))
    labels[merged[merged.RMSD<=2].index]=1
    merged.IF_dist_peptide = 1/merged.IF_dist_peptide #Invert to make higher low
    merged.IF_dist_receptor = 1/merged.IF_dist_receptor #Invert to make higher low
    merged.esm_prob = 1/merged.esm_prob #Invert to make higher low
    merged['Positive']=labels
    #Combine receptor IF and plDDT
    merged['IF_dist_receptor*plDDT']=merged.IF_dist_receptor*merged.peptide_plDDT
    #Go through features and create ROC
    nicer_names = {'IF_dist_receptor':'receptor IF dist', 'peptide_plDDT':'plDDT',
                'IF_dist_receptor*plDDT':'receptor IF dist⋅plDDT'
                }
    colors = {'peptide_plDDT':'tab:green',
               'IF_dist_receptor':'tab:blue',
              'IF_dist_receptor*plDDT':'grey'}


    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    for feature in nicer_names.keys():
        #Create ROC
        fpr, tpr, threshold = metrics.roc_curve(merged.Positive.values, merged[feature].values, pos_label=1)
        roc_auc = metrics.auc(fpr, tpr)
        plt.plot(fpr, tpr, label =nicer_names[feature]+': %0.2f' % roc_auc, color=colors[feature])

    plt.plot([0,1],[0,1],linewidth=1,linestyle='--',color='grey')
    plt.legend(fontsize=9)
    plt.title('ROC for AF prediction accuracy')
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'ROC_prediction_accuracy.png',format='png',dpi=300)
    plt.close()


    #Plot plDDT and dist vs RMSD
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(merged.RMSD, merged.peptide_plDDT,s=2)
    plt.xlabel('RMSD')
    plt.ylabel('plDDT')
    plt.xscale('log')
    plt.title('plDDT vs RMSD')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'plddt_rmsd.png',format='png',dpi=300)
    plt.close()
    print('Spearman RMSD vs plDDT:',spearmanr(merged.RMSD, merged.peptide_plDDT)[0])

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(merged.RMSD, 1/merged.IF_dist_receptor,s=2)
    plt.xlabel('RMSD')
    plt.ylabel('Receptor IF dist')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(0,70)
    plt.ylim(0,70)
    plt.title('Receptor IF dist vs RMSD')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'receptor_IF_dist_rmsd.png',format='png',dpi=300)
    plt.close()
    print('Spearman RMSD vs IF_dist_receptor:',spearmanr(merged.RMSD,1/merged.IF_dist_receptor)[0])



def analyse_mutated_peptides(mutated_peptide_plddt8, mutated_peptide_if_dists8, mut_peptide_seq_blosum62, outdir):
    '''Analyse the mutated peptides
    '''

    #Merge
    merged = pd.merge(mutated_peptide_plddt8, mutated_peptide_if_dists8,on='ID')
    merged = pd.merge(merged, mut_peptide_seq_blosum62,on='ID')

    #Get number of mutated pos
    merged['num_mut_pos']=[int(x.split('_')[2]) for x in merged.ID.values]
    merged['sample']=[x.split('_')[3] for x in merged.ID.values]
    merged['PDB']=[x.split('_')[0] for x in merged.ID.values]
    merged['seqlen']=[len(x) for x in merged.Sequence.values]
    #Get the total number of if pos
    tot_if_pos = {'PDB':[],'n_if_pos':[]}
    for pdbid in merged.PDB.unique():
        sel = merged[merged.PDB==pdbid]
        tot_if_pos['PDB'].append(pdbid)
        tot_if_pos['n_if_pos'].append(sel.num_mut_pos.max())
    #create df
    tot_if_pos = pd.DataFrame.from_dict(tot_if_pos)
    merged = pd.merge(merged, tot_if_pos, on='PDB',how='left')
    merged['mut_fraction']=merged.num_mut_pos/merged.seqlen

    #Plot
    fig,ax = plt.subplots(figsize=(12/2.54,9/2.54))
    plt.scatter(merged.mut_fraction, merged.IF_dist_receptor,s=2,c=merged.plDDT)
    plt.xlabel('Mutation fraction')
    plt.ylabel('Receptor IF distance (Å)')
    plt.ylim([4,10])
    plt.title('Interface distance vs Mutation fraction')
    cbar = plt.colorbar()
    cbar.set_label('plDDT')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'receptor_IF_dist_mut_frac.png',format='png',dpi=300)
    plt.close()

    #plDDT
    merged = merged.rename(columns={'mut_fraction':'Mutation fraction'})
    fig,ax = plt.subplots(figsize=(12/2.54,9/2.54))
    sns.jointplot(merged['Mutation fraction'], merged.plDDT, kind='kde')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'plDDT_mut_frac.png',format='png',dpi=300)
    plt.close()
    print('Average plDDT of the mutated binders',merged.plDDT.mean())

    #Plot per peptide
    for pdbid in merged.PDB.unique():
        sel = merged[merged.PDB==pdbid]
        fig,ax = plt.subplots(figsize=(12/2.54,9/2.54))
        plt.scatter(sel['Mutation fraction'],sel.IF_dist_receptor,s=2,c=sel.plDDT)
        plt.xlabel('Mutation fraction')
        plt.ylabel('Receptor IF dist (Å)')
        #plt.ylim([4,10])
        plt.title(pdbid)
        cbar = plt.colorbar()
        cbar.set_label('plDDT')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        plt.tight_layout()
        plt.savefig(outdir+pdbid+'_receptor_IF_dist_mut_frac.png',format='png',dpi=300)
        plt.close()






def analyse_binder_opt(binder_opt_dir, native_peptide_fastadir, native_pdbdir, outdir):
    '''Analyse the binder optimisation
    '''

    files = glob.glob(binder_opt_dir+'*_sequence.npy')
    contact_sim, losses, plddts, rasas, ids = [], [], [], [], []
    print('ID niter loss plDDT seqlen design_seq native_seq contact_sim RASA')
    for name in files:
        sequences = np.load(name)[1:]
        pdbid = name.split('/')[-1].split('_')[0]
        loss = np.load(binder_opt_dir+pdbid+'_loss.npy')[1:] #Loss and plddt will have the init at pos 0
        plddt = np.load(binder_opt_dir+pdbid+'_plddt.npy')[1:]
        design_contac_sim = pd.read_csv(binder_opt_dir+pdbid+'_contact_similarities.csv')
        design_contac_sim = design_contac_sim.sort_values(by='ID')
        rasa = pd.read_csv(binder_opt_dir+pdbid+'_RASA.csv')
        rasa = rasa.sort_values(by='model_id')
        native_seq = pd.read_csv(glob.glob(native_peptide_fastadir+pdbid+'*.fasta')[0], sep='\n')
        #Position of best loss
        sel_pos = np.argmin(loss)

        #sel_pos = np.argmax(plddt)
        print(pdbid, sel_pos, min(loss), plddt[sel_pos], len(native_seq.values[0][0]),
        sequences[sel_pos], native_seq.values[0][0], design_contac_sim.frac_sim.values[sel_pos], rasa.RASA.values[sel_pos])
        contact_sim.extend([*design_contac_sim.frac_sim.values])
        losses.extend([*loss])
        plddts.extend([*plddt])
        rasas.extend([*rasa.RASA.values/max(rasa.RASA)])
        ids.append(pdbid)

        if len(contact_sim)!=len(losses):
            pdb.set_trace()

    #Plot the loss curves
    fig,ax = plt.subplots(figsize=(24/2.54,12/2.54))
    step = 50
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'magenta', 'tab:purple', 'tab:brown',
            'tab:pink', 'tab:grey', 'tab:olive', 'tab:cyan', 'k', 'teal']
    for i in range(0,len(losses),1000):
        x_ra, y_ra = [], []
        loss = losses[i:i+1000]
        for j in range(0,1000,step):
            x_ra.append(j+step/2)
            y_ra.append(np.median(loss[j:j+step]))
        plt.plot(x_ra, y_ra, label=ids[int(i/1000)], color=colors[int(i/1000)])
    plt.legend()
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.title('Loss per iteration')
    plt.legend()
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'iteration_loss.png',format='png',dpi=300)
    plt.close()

    #Plot loss vs contact sim
    #RA
    losses = np.array(losses)
    x_ra, y_ra = [], []
    step=0.05
    for t in np.arange(0,1+step,step):
        sel = losses[np.argwhere((contact_sim>=t)&(contact_sim<t+step))[:,0]]
        if len(sel)>0:
            x_ra.append(t)
            y_ra.append(np.median(sel))
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.plot(x_ra, y_ra, label = 'Median', linestyle='-.', linewidth=1, color='royalblue')
    print(len(losses))

    #sns.kdeplot(contact_sim, losses, cmap='Blues',kind='hex')
    plt.scatter(contact_sim, losses, s=0.5,alpha=0.25, marker='+', c=plddts)
    cbar = plt.colorbar()
    cbar.set_label('plDDT')
    plt.xlabel('Contact similarity')
    plt.ylabel('Loss')
    plt.title('Contact similarity and loss')
    plt.legend()
    #plt.ylim([0.001,10])
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'contact_sim_loss.png',format='png',dpi=300)
    plt.close()

    #Plot RASA vs loss
    #RA
    losses = np.array(losses)
    x_ra, y_ra = [], []
    step=0.05
    for t in np.arange(0,1+step,step):
        sel = losses[np.argwhere((rasas>=t)&(rasas<t+step))[:,0]]
        if len(sel)>0:
            x_ra.append(t)
            y_ra.append(np.median(sel))
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.plot(x_ra, y_ra, label = 'Median', linestyle='-.', linewidth=1, color='royalblue')
    plt.scatter(rasas, losses, s=0.5,alpha=0.25, marker='+', c=plddts)
    cbar = plt.colorbar()
    cbar.set_label('plDDT')
    plt.xlabel('normalised RASA')
    plt.ylabel('Loss')
    plt.title('RASA and loss')
    plt.legend()
    plt.ylim([0.001,10])
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'rasa_loss.png',format='png',dpi=300)
    plt.close()

    pdb.set_trace()



def plot_interacting_residues(binder_opt_dir, native_pdbdir, outdir):
    '''Visualise the contacts
    '''

    hp = 'tab:gray' #hydrophobic color
    small = 'mediumseagreen'
    polar = 'k'
    neg = 'magenta'
    pos = 'royalblue'
    AA_color_scheme = { 'A':hp,'R':pos,'N':polar,'D':neg,'C':polar,'E':neg,
                    'Q':polar,'G':small,'H':pos,'I':hp,'L':hp,'K':pos,
                    'M':hp,'F':hp,'P':hp,'S':polar,'T':polar,'W':hp,
                    'Y':hp,'V':hp
                  }

    interacting_residues = glob.glob(binder_opt_dir+'PDB/*_interacting_residues.csv')
    for name in interacting_residues:
        pdbid = name.split('/')[-1].split('_')[0]
        design_ints = pd.read_csv(name)
        native_ints = pd.read_csv(native_pdbdir+pdbid+'_interacting_residues.csv')

        fig,ax = plt.subplots(figsize=(28/2.54,9/2.54))
        #Plot the design
        xi=0
        for ind, row in design_ints.iterrows():
            yi=0
            for char in row.Peptide_res:
                plt.text(row.Receptor_resno, yi,char, color=AA_color_scheme[char], fontsize=10)
                yi+=0.1
            xi+=5

        #Plot the native
        xi=0
        for ind, row in native_ints.iterrows():
            yi=1
            for char in row.Peptide_res:
                plt.text(row.Receptor_resno, yi,char, color=AA_color_scheme[char], fontsize=10)
                yi-=0.1
            xi+=5

        plt.xlim([min(native_ints.Receptor_resno),max(native_ints.Receptor_resno)])
        #plt.xticks(ticks=, labels=native_ints.Receptor_resno)
        ax.spines['top'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.spines['right'].set_visible(False)
        plt.title(pdbid)
        plt.yticks(ticks=[0,1],labels=['Design', 'Native'])
        plt.xlabel('Receptor residue')
        plt.tight_layout()
        plt.savefig(outdir+pdbid+'_text.png',format='png',dpi=300)
        plt.close()

def analyse_affinity(prm_meta, prm_plddt, prm_num_contacts, outdir):
    '''Analyse the affinity
    '''

    #Merge
    prm_plddt = prm_plddt.sort_values(by='ID')
    prm_num_contacts = prm_num_contacts.sort_values(by='ID')
    prm_meta = prm_meta.loc[:len(prm_plddt)-1]
    prm_meta['plDDT']=prm_plddt.peptide_plDDT.values
    prm_meta['num_contacts']=prm_num_contacts.num_contacts.values
    prm_meta['plDDT⋅log(contacts)']=np.log10(prm_meta.num_contacts.values+1)*prm_meta.plDDT.values

    #Plot
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    prm_meta = prm_meta.rename(columns={'ELISA_ratio':'ELISA ratio'})
    sns.jointplot(prm_meta['ELISA ratio'], prm_meta['plDDT'],kind='kde')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'plddt_elisa.png',format='png',dpi=300)
    plt.close()


    low_plddt_contacts, high_plddt_contacts  = [], []
    for rid in prm_meta.receptor_id.unique():
        sel = prm_meta[prm_meta.receptor_id==rid]
        fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
        plt.scatter(sel['ELISA ratio'], sel['plDDT'])
        plt.title(rid)
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        plt.ylabel('plDDT')
        plt.xlabel('ELISA ratio')
        plt.tight_layout()
        plt.savefig(outdir+rid+'_plddt_elisa.png',format='png',dpi=300)
        plt.close()


def analyse_alt_binding_sites(plDDT_only_optdir, outdir):
    '''Analyse if alternative binding sites can be captured
    '''

    losses = {}
    for i in range(1,4):
        losses[i] = np.load(plDDT_only_optdir+'loss_'+str(i)+'.npy')

    sel_df = {'init':[], 'id':[], 'loss':[]} #Save the best designs
    for key in losses:
        sel_inds = np.argsort(losses[key])[:50]
        sel_df['init'].extend([key]*len(sel_inds))
        sel_df['id'].extend([*sel_inds])
        sel_df['loss'].extend([*losses[key][sel_inds]])

    sel_df = pd.DataFrame.from_dict(sel_df)
    #Save
    sel_df.to_csv(plDDT_only_optdir+'best_designs.csv', index=False)


def analyse_str_flex(all_CB_receptor_rmsd, binder_opt_dir, outdir):
    '''Analyse the structural variability of the receptor during the opt
    '''

    all_CB_receptor_rmsd['iter']=[int(x.split('_')[1]) for x in all_CB_receptor_rmsd.ID.values]
    all_CB_receptor_rmsd['ID']=[x.split('_')[0] for x in all_CB_receptor_rmsd.ID.values]

    all_rmsd, all_sim = [], []
    for pdbid in all_CB_receptor_rmsd.ID.unique():
        sel = all_CB_receptor_rmsd[all_CB_receptor_rmsd.ID==pdbid]
        sel = sel.sort_values(by='iter')
        design_contac_sim = pd.read_csv(binder_opt_dir+pdbid+'_contact_similarities.csv')
        design_contac_sim = design_contac_sim.sort_values(by='ID')
        sel['frac_sim']=design_contac_sim.frac_sim.values
        all_rmsd.extend([*sel.CB_RMSD.values])
        all_sim.extend([*sel.frac_sim.values])
        print(pdbid, sel.CB_RMSD.max()-sel.CB_RMSD.min(), np.argmax(sel.CB_RMSD.values), np.argmin(sel.CB_RMSD.values))
    #Plot
    cat_df = pd.DataFrame()
    cat_df['CB RMSD'] = all_rmsd
    cat_df['Contact similarity'] = all_sim
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    x_ra, y_ra = [], []
    step=0.1
    for t in np.arange(min(cat_df['CB RMSD']),max(cat_df['CB RMSD']), step):
        sel = cat_df[(cat_df['CB RMSD']>t)&(cat_df['CB RMSD']<=t+step)]
        x_ra.append(t+step/2)
        y_ra.append(np.median(sel['Contact similarity']))

    #sns.jointplot(cat_df['CB RMSD'], cat_df['Contact similarity'], kind='kde')
    ax.plot(x_ra, y_ra, linestyle='--', color='k', label='Median')
    plt.scatter(all_rmsd, all_sim, alpha=0.2, s=1)
    sns.kdeplot(all_rmsd, all_sim)
    plt.xlabel('CB RMSD')
    plt.ylabel('Contact similarity')
    plt.title('Structural adaptation')
    plt.legend()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'str_sim.png',format='png',dpi=300)
    plt.close()

def create_logo(if_pos, aa_freqs, outdir, pdbid):
    '''Generate an aa logo
    '''
    amino_acids = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P',
       'Q', 'R', 'S', 'T', 'V', 'W', 'Y']

    hp = 'tab:gray' #hydrophobic color
    small = 'mediumseagreen'
    polar = 'darkgray'
    neg = 'magenta'
    pos = 'royalblue'
    AA_color_scheme = { 'A':hp,'R':pos,'N':polar,'D':neg,'C':polar,'E':neg,
                    'Q':polar,'G':small,'H':pos,'I':hp,'L':hp,'K':pos,
                    'M':hp,'F':hp,'P':hp,'S':polar,'T':polar,'W':hp,
                    'Y':hp,'V':hp
                  }
    #Convert to df
    aa_seq_df = pd.DataFrame(aa_freqs,columns = amino_acids)
    #Transform
    aa_seq_df =logomaker.transform_matrix(aa_seq_df,from_type='probability',to_type='information')

    #Logo
    aa_logo = logomaker.Logo(aa_seq_df, color_scheme=AA_color_scheme)
    plt.ylabel('Information (bits)')
    plt.xticks(np.arange(len(if_pos)), labels=if_pos, rotation=60)
    plt.title(pdbid)
    plt.tight_layout()
    plt.savefig(outdir+pdbid+'_if_logo.png',format='png',dpi=300)
    plt.close()

def analyse_convergence(binder_opt_dir, native_pdbdir, outdir):
    '''Analyse the convergence between the three different runs
    '''

    binder_opt_dir = '/'.join(binder_opt_dir.split('/')[:-2])


    seqs = glob.glob(binder_opt_dir+'/init3'+'/*_sequence.npy')
    for name in seqs:
        sequences = np.load(name)[1:]
        pdbid = name.split('/')[-1].split('_')[0]
        #Get logo freqs
        freqs = {}
        for ninit in ['3', '4', '5']:
            freqs[ninit] = pd.read_csv(binder_opt_dir+'/init'+ninit+'/'+pdbid+'_logo_freqs.csv')

        #Load native
        native_logo = pd.read_csv(native_pdbdir+pdbid+'_logo_freqs.csv')
        freqs['2']=native_logo
        #Get all unique positions
        unique_pos = []
        for ninit in freqs:
            freqs[ninit] = freqs[ninit].rename(columns={'Unnamed: 0': 'if_pos'})
            freqs[ninit] = freqs[ninit].sort_values(by='if_pos')
            unique_pos.extend([*freqs[ninit]['if_pos'].values])

        unique_pos = np.unique(unique_pos)
        #Add the unique_pos to the dfs so all contain the same
        for ninit in freqs:
            df = freqs[ninit]
            for upos in unique_pos:
                if upos in df.if_pos.values:
                    continue
                else:
                    df = df.append(df.loc[0])
                    df = df.reset_index()
                    df.loc[len(df)-1, df.columns[2:]]=0
                    df.loc[len(df)-1, df.columns[1]]=upos
                    df = df[df.columns[1:]]
            df = df.sort_values(by='if_pos')

            #Logo
            df[df.columns[1:]]+=0.001
            create_logo(np.array(df.if_pos.values, dtype=int), df[df.columns[1:]].values, outdir+'logos/', pdbid+'_'+str(int(ninit)-2))



def analyse_motif_search(motif_dir, motif_meta, outdir):
    '''Create contact logos for the motif search
    '''

    logo_freqs = glob.glob(motif_dir+'*_sequence.npy')

    for name in logo_freqs:
        pdbid = name.split('/')[-1].split('_')[0]
        #Get motif
        motif_sel = motif_meta[motif_meta.pdb_id==pdbid]
        motif, pepseq = motif_sel.Motif.values[0], motif_sel.peptide_seq.values[0]
        #Get motif start end

        for si in range(len(pepseq)-len(motif)):
            if pepseq[si:si+len(motif)]==motif:
                break
        motif_inds = np.arange(si,si+len(motif))
        #Get logo freqs
        freqs = {}
        freqs['design'] = pd.read_csv(motif_dir+'/'+pdbid+'_logo_freqs.csv')
        #Load native
        freqs['native'] = pd.read_csv(motif_dir+'/PDB/'+pdbid+'_logo_freqs.csv')

        #Check which ints are in the motif
        int_res = pd.read_csv(motif_dir+'/PDB/'+pdbid+'_interacting_residues.csv')
        receptor_motif_pos = []
        for ind,row in int_res.iterrows():
            pep_resnos = [int(x) for x in row.Peptide_resno.split('-')]
            if np.intersect1d(pep_resnos, motif_inds).shape[0]>0:
                receptor_motif_pos.append(True)
            else:
                receptor_motif_pos.append(False)
        int_res['motif_pos']=receptor_motif_pos
        receptor_motif_pos = int_res[int_res.motif_pos==True]['Receptor_resno'].values

        #Get all unique positions
        unique_pos = []
        for ninit in freqs:
            freqs[ninit] = freqs[ninit].rename(columns={'Unnamed: 0': 'if_pos'})
            freqs[ninit] = freqs[ninit].sort_values(by='if_pos')
            unique_pos.extend([*freqs[ninit]['if_pos'].values])

        unique_pos = np.unique(unique_pos)
        #Add the unique_pos to the dfs so all contain the same
        normalised_freqs = {}
        for ninit in freqs:
            df = freqs[ninit]
            for upos in unique_pos:
                if upos in df.if_pos.values:
                    continue
                else:
                    df = df.append(df.loc[0])
                    df = df.reset_index()
                    df.loc[len(df)-1, df.columns[2:]]=0
                    df.loc[len(df)-1, df.columns[1]]=upos
                    df = df[df.columns[1:]]

            df = df.sort_values(by='if_pos')
            #Get only motif pos
            df = df[df.if_pos.isin(receptor_motif_pos)]
            #Logo
            df[df.columns[1:]]+=0.001
            #create_logo(np.array(df.if_pos.values, dtype=int), df[df.columns[1:]].values, outdir+'logos/', pdbid+'_motif_'+ninit)

            #Normalise
            df_vals = df.values[:,1:]
            for i in range(len(df_vals)):
                df_vals[i]/=max(df_vals[i])
                df_vals[i] = df_vals[i]/np.sum(df_vals[i])

            #Save
            normalised_freqs[ninit]=df_vals

        #Calculate the logo similarity
        freq_match, perfect_match = logo_similarity(normalised_freqs['design'], normalised_freqs['native'], pdbid)
        print(pdbid, freq_match, perfect_match, freq_match/perfect_match)
        #Get the motif positions from the native interacting_residues
        #print(pdbid, receptor_motif_pos)


def logo_similarity(aa_freq1, aa_freq2, pdbid):
    '''Calculate the logo similarity btw aafreq1 and aafre12
    '''

    pos_sim, perfect_sim = [], []
    for i in range(len(aa_freq1)):
        f1, f2 = aa_freq1[i], aa_freq2[i]
        pos_sim.append(np.sum(f1*f2))
        #Get the perfect match prob
        f1, f2 = aa_freq1[i], aa_freq1[i]
        perfect_sim.append(np.sum(f1*f2))

    return np.average(pos_sim), np.average(perfect_sim)

def analyse_minibinder_seqs(mini_binder_dir, outdir):
    '''Analyse the mini binders
    '''

    mini_losses = glob.glob(mini_binder_dir+'*_loss.csv')
    all_losses, all_counts, all_plddt = [], [], []
    for name in mini_losses:
        loss = pd.read_csv(name)
        loss['index']=[int(x.split('_')[1]) for x in loss.id]
        id = name.split('/')[-1].split('_')[0]
        selected_seqs = pd.read_csv(mini_binder_dir+id+'_selected_seqs.csv')
        loss = loss.sort_values(by='index')
        loss['NGS_counts']=selected_seqs[selected_seqs.columns[-3]].values
        loss['NGS_counts'] /= max(loss['NGS_counts'].values)
        loss['loss']= (loss.if_dist_binder+loss.if_dist_receptor)/2*1/loss.plddt*loss.delta_CM
        all_losses.extend([*loss.loss.values])
        all_counts.extend([*loss.NGS_counts.values])
        all_plddt.extend([*loss.plddt.values])
        #Print the best and worst models
        sel = loss[loss.NGS_counts>0]
        print(id, np.argmin(sel.loss.values), sel.NGS_counts.values[np.argmin(sel.loss.values)], np.argmax(loss.loss.values), loss.NGS_counts.values[np.argmax(loss.loss.values)])

    #Group the NGS thresholds
    ngs_count_df = pd.DataFrame()
    ngs_count_df['Loss']=all_losses
    ngs_count_df['Normalised NGS counts']=all_counts
    ngs_count_df['plddt']=all_plddt
    pdb.set_trace()
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    g = sns.jointplot(ngs_count_df['Normalised NGS counts'], ngs_count_df['Loss'], kind='kde') #, xlim=[0,0.13], ylim=[0,13])
    #plt.scatter(all_counts, all_losses,s=0.5,alpha=0.25, marker='+', c=all_plddt)
    plt.savefig(outdir+'ngs_counts.png',format='png',dpi=300)
    plt.close()


    print('Average minibinder plDDT', np.average(all_plddt))
    print('Number with NGS count 0 =',len(ngs_count_df[ngs_count_df['Normalised NGS counts']==0]))
    #ROC using 0.01 as positives
    ngs_count_df['Binder']=0
    ngs_count_df.at[ngs_count_df[ngs_count_df['Normalised NGS counts']>0.01].index, 'Binder']=1

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    #Create ROC
    fpr, tpr, threshold = metrics.roc_curve(ngs_count_df.Binder.values, 1/ngs_count_df.Loss.values, pos_label=1)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label ='Loss'+': %0.2f' % roc_auc, color='tab:blue')

    plt.plot([0,1],[0,1],linewidth=1,linestyle='--',color='grey')
    plt.legend(fontsize=9)
    plt.title('ROC for binder selection')
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'ROC_minibinder_sel.png',format='png',dpi=300)
    plt.close()

    print(tpr[np.argwhere(fpr<=.2)[-1][0]], 'of models can be selected at a FPR of 20%.')
    print(tpr[np.argwhere(fpr<=.1)[-1][0]], 'of models can be selected at a FPR of 10%.')

#################MAIN####################

#Parse args
args = parser.parse_args()
#Get data
LNR_set = pd.read_csv(args.LNR_set[0])
native_lm_probs = pd.read_csv(args.native_lm_probs[0])
native_peptide_if_rmsd = pd.read_csv(args.native_peptide_if_rmsd[0])
native_peptide_plddt8 = pd.read_csv(args.native_peptide_plddt8[0])
native_peptide_if_dists8 = pd.read_csv(args.native_peptide_if_dists8[0])
mutated_peptide_plddt8 = pd.read_csv(args.mutated_peptide_plddt8[0])
mutated_peptide_if_dists8 = pd.read_csv(args.mutated_peptide_if_dists8[0])
mut_peptide_seq_blosum62 = pd.read_csv(args.mut_peptide_seq_blosum62[0])
#binder_opt_dir = args.binder_opt_dir[0]
native_peptide_fastadir = args.native_peptide_fastadir[0]
native_pdbdir = args.native_pdbdir[0]
prm_meta = pd.read_csv(args.prm_meta[0])
prm_plddt = pd.read_csv(args.prm_plddt[0])
prm_num_contacts = pd.read_csv(args.prm_num_contacts[0])
plDDT_only_optdir = args.plDDT_only_optdir[0]
#all_CB_receptor_rmsd = pd.read_csv(args.all_CB_receptor_rmsd[0])
motif_dir = args.motif_dir[0]
motif_meta = pd.read_csv(args.motif_meta[0])
mini_binder_dir = args.mini_binder_dir[0]
outdir = args.outdir[0]
#Analyse rmsd
#native_rmsd_num_recycles(native_peptide_if_rmsd, outdir)
#Analyse TP identification
#distinguish_native(native_peptide_if_rmsd, native_peptide_if_dists8 , native_lm_probs, native_peptide_plddt8, outdir)
#Analyse the mutations
#analyse_mutated_peptides(mutated_peptide_plddt8, mutated_peptide_if_dists8, mut_peptide_seq_blosum62, outdir)
#Analyse the binder opt
#analyse_binder_opt(binder_opt_dir, native_peptide_fastadir, native_pdbdir, outdir)
#plot_interacting_residues(binder_opt_dir, native_pdbdir, outdir)
#analyse_affinity(prm_meta, prm_plddt, prm_num_contacts, outdir)
#analyse_alt_binding_sites(plDDT_only_optdir, outdir)
#analyse_str_flex(all_CB_receptor_rmsd, binder_opt_dir, outdir)
#analyse_convergence(binder_opt_dir, native_pdbdir, outdir)
#analyse_motif_search(motif_dir, motif_meta, outdir)
analyse_minibinder_seqs(mini_binder_dir, outdir)
