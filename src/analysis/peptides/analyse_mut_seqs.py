import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import Counter
import pdb


parser = argparse.ArgumentParser(description = '''Analyse the mutated sequences.''')
parser.add_argument('--sequences', nargs=1, type= str, default=sys.stdin, help = 'Path to mutated sequences')
parser.add_argument('--native_seq', nargs=1, type= str, default=sys.stdin, help = 'Native sequence')
parser.add_argument('--BLOSUM62', nargs=1, type= str, default=sys.stdin, help = 'BLOSUM62 matrix')

##############FUNCTIONS##############
def calc_blosum_score(sequences, native_seq, BLOSUM62):
    '''Score the sequences towards the native seq
    using the BLOSUM62 matrix
    '''

    #Get array of each seq
    seq_arrays = []
    for i in range(len(sequences)):
        seq_arrays.append(np.array([x for x in sequences[i]]))

    seq_arrays = np.array(seq_arrays)
    seq_scores = []
    for i in range(len(native_seq)):
        dists = BLOSUM62[BLOSUM62.AA1==native_seq[i]] #Get all dists for aa i
        #Get scores (dists) for column
        col_scores = [dists[x].values[0] for x in seq_arrays[:,i]]
        seq_scores.append(col_scores)
    #Array
    seq_scores = np.array(seq_scores)
    #Return average for each seq
    return np.average(seq_scores,axis=0)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
sequences = pd.read_csv(args.sequences[0])
native_seq = args.native_seq[0]
BLOSUM62 = pd.read_csv(args.BLOSUM62[0])

#BLOSUM62
blosum_scores = calc_blosum_score(sequences.peptide_sequence.values, native_seq, BLOSUM62)
#Save
sequences['avg_BLOSUM62']=blosum_scores
sequences.to_csv(args.sequences[0],index=None)
