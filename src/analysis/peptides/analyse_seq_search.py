import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import Counter
import pdb


parser = argparse.ArgumentParser(description = '''Analyse the sequence search for the directed evolution.''')
parser.add_argument('--sequences', nargs=1, type= str, default=sys.stdin, help = 'Path to sequences searched')
parser.add_argument('--native_seq', nargs=1, type= str, default=sys.stdin, help = 'Native sequence')
parser.add_argument('--BLOSUM62', nargs=1, type= str, default=sys.stdin, help = 'BLOSUM62 matrix')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Outname of df containing sequence comparison')

##############FUNCTIONS##############
def seq_to_int(sequences):
    '''Map all seqs to ints
    '''

    restypes = {'A':0, 'R':1, 'N':2, 'D':3, 'C':4, 'Q':5,
                'E':6, 'G':7, 'H':8, 'I':9, 'L':10,
                'K':11, 'M':12, 'F':13, 'P':14, 'S':15,
                'T':16, 'W':17, 'Y':18, 'V':19 }

    int_reps = []

    for seq in sequences:
        int_reps.append([restypes[x] for x in seq])

    return np.array(int_reps)

def calc_seq_id(seqs_as_ints, native_as_int):
    '''Calculate the sequence identity
    '''

    matches = np.argwhere((seqs_as_ints-native_as_int)==0)
    #Count matches
    counted = Counter(matches[:,0])
    seq_id = np.zeros(len(seqs_as_ints))
    for key in counted:
        seq_id[key]=counted[key]
    seq_id /= native_as_int.shape[0]

    return seq_id

def calc_blosum_score(sequences, native_seq, BLOSUM62):
    '''Score the sequences towards the native seq
    using the BLOSUM62 matrix
    '''

    #Get array of each seq
    seq_arrays = []
    for i in range(len(sequences)):
        seq_arrays.append(np.array([x for x in sequences[i]]))

    seq_arrays = np.array(seq_arrays)
    seq_scores = []
    for i in range(len(native_seq)):
        dists = BLOSUM62[BLOSUM62.AA1==native_seq[i]] #Get all dists for aa i
        #Get scores (dists) for column
        col_scores = [dists[x].values[0] for x in seq_arrays[:,i]]
        seq_scores.append(col_scores)
    #Array
    seq_scores = np.array(seq_scores)
    #Return average for each seq
    return np.average(seq_scores,axis=0)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
sequences = np.load(args.sequences[0])
native_seq = args.native_seq[0]
BLOSUM62 = pd.read_csv(args.BLOSUM62[0])
outname = args.outname[0]
#Map to ints
seqs_as_ints = seq_to_int(sequences)
native_as_int = seq_to_int([native_seq])[0]
#Sequence identity
seq_ids = calc_seq_id(seqs_as_ints, native_as_int)

#BLOSUM62
blosum_scores = calc_blosum_score(sequences, native_seq, BLOSUM62)

#Save
np.save(outname+'_seqid.npy', seq_ids)
#Save
np.save(outname+'_blosum62.npy', blosum_scores)
