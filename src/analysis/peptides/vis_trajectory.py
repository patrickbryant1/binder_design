import sys
import os
import pdb

import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
from matplotlib.animation import FuncAnimation
from matplotlib import animation
from Bio.SVDSuperimposer import SVDSuperimposer
import argparse


parser = argparse.ArgumentParser(description = '''Visualize sequence search''')

parser.add_argument('--losses', nargs=1, type= str, required=True, help = "losses during sequence search")
parser.add_argument('--if_res', nargs=1, type= str, required=True, help = "Interface residues")
parser.add_argument('--pdbdir', nargs=1, type= str, required=True, help = "Path to directory with PDB files")
parser.add_argument('--outdir', nargs=1, type= str, help = 'Outdir.')


#################FUNCTIONS#################

def select_trajectory(losses, interval):
    '''Select a trajectory of losses starting at the highest
    '''

    order = np.argsort(losses)
    sel = np.arange(0,len(order),interval)
    sampled_order = order[sel]
    sampled_losses = losses[sampled_order]

    return sampled_order, sampled_losses

def parse_atm_record(line):
    '''Get the atm record
    '''
    record = defaultdict()
    record['name'] = line[0:6].strip()
    record['atm_no'] = int(line[6:11])
    record['atm_name'] = line[12:16].strip()
    record['atm_alt'] = line[17]
    record['res_name'] = line[17:20].strip()
    record['chain'] = line[21]
    record['res_no'] = int(line[22:26])
    record['insert'] = line[26].strip()
    record['resid'] = line[22:29]
    record['x'] = float(line[30:38])
    record['y'] = float(line[38:46])
    record['z'] = float(line[46:54])
    record['occ'] = float(line[54:60])
    record['B'] = float(line[60:66])

    return record

def read_pdb(pdbfile):
    '''Read a pdb file per chain
    '''
    pdb_chains = {}
    res_nos = {}
    pdb_seqs = {}
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    with open(pdbfile) as file:
        for line in file:
            if line.startswith('ATOM'):
                record = parse_atm_record(line)
                if record['chain'] in [*pdb_chains.keys()]:

                    if record['atm_name']=='CA':
                        pdb_seqs[record['chain']] += three_to_one[record['res_name']]
                        pdb_chains[record['chain']].append([record['x'],record['y'],record['z']])
                        res_nos[record['chain']].append(record['res_no'])
                else:
                    pdb_chains[record['chain']] = [[record['x'],record['y'],record['z']]]
                    res_nos[record['chain']] = [record['res_no']]
                    pdb_seqs[record['chain']] = ''


    return pdb_chains, res_nos, pdb_seqs




def plot_trajectory(pdbdir, sampled_order, if_res, outdir):
    '''Plot the trajectory
    '''
    #Inverse
    sampled_order = sampled_order[::-1]
    #sampled_order = sampled_order[-30:]
    #Set the reference frame
    #Set the coordinates to be superimposed.
    #coords will be put on top of reference_coords.
    pdb_chains, res_nos, pdb_seqs = read_pdb(pdbdir+'unrelaxed_'+str(sampled_order[0])+'.pdb')
    reference_coords = np.array(pdb_chains['A'])
    sup = SVDSuperimposer()

    #Get interface

    chA_if_pos = []
    for resno in if_res:
        chA_if_pos.extend([*np.argwhere(res_nos['A']==resno)[:,0]])
    chA_if_pos = np.array(chA_if_pos)
    chA_non_if_pos = np.setdiff1d(np.arange(len(res_nos['A'])),chA_if_pos)

    def animate(i):

        if os.path.exists(pdbdir+'unrelaxed_'+str(sampled_order[i])+'.pdb'):
            print(i)
            ax.clear()
            pdb_chains, res_nos, pdb_seqs = read_pdb(pdbdir+'unrelaxed_'+str(sampled_order[i])+'.pdb')

            chA, chB = np.array(pdb_chains['A']), np.array(pdb_chains['B'])
            #Rotate
            #Set the coordinates to be superimposed.
            #coords will be put on top of reference_coords.

            sup.set(reference_coords,chA) #(reference_coords, coords)
            sup.run()
            rot, tran = sup.get_rotran()

            #Rotate coords from new chain to its new relative position/orientation
            chA = np.dot(chA, rot) + tran
            chA_if = chA[chA_if_pos]
            chA_non_if = chA[chA_non_if_pos]
            chB = np.dot(chB, rot) + tran

            ax.plot3D(chA_non_if[:,0],chA_non_if[:,1],chA_non_if[:,2], c='grey', linewidth=5, alpha=0.5)
            #If
            ax.plot3D(chA_if[:,0],chA_if[:,1],chA_if[:,2], c='orange', linewidth=5)
            #peptide
            ax.plot3D(chB[:,0],chB[:,1],chB[:,2], c='cyan',  linewidth=5)
            plt.title('Sample '+str(i))
            plt.axis('off')
            plt.grid(b=None)
        else:
            print('No file.')


    fig = plt.figure()
    #plt.style.use("dark_background")
    ax = plt.axes(projection='3d')
    ani = FuncAnimation(fig,animate,frames=len(sampled_order), interval=1)

    #with open("pdb_animation.html", "w") as f:
    #print(ani.to_html5_video(), file=f)

    writergif = animation.PillowWriter(fps=5)
    ani.save(outdir+'pdb_animation.gif', writer=writergif)


#################MAIN####################

#Parse args
args = parser.parse_args()
#Get data
losses = np.load(args.losses[0])
if_res = np.load(args.if_res[0])
pdbdir = args.pdbdir[0]
outdir = args.outdir[0]
print(np.argmin(losses))
sampled_order, sampled_losses = select_trajectory(losses, 5)
plot_trajectory(pdbdir, sampled_order, if_res, outdir)
