import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb


parser = argparse.ArgumentParser(description = '''Calculate a contact logo based on the contact similarity between designed peptide sequences and the receptor.''')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--design_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory containing csvs with interacting residues for the designed peptides.')
parser.add_argument('--losses', nargs=1, type= str, default=sys.stdin, help = 'Path to losses.')
parser.add_argument('--fraction', nargs=1, type= float, default=sys.stdin, help = 'How large a fraction of the designs to use.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')

##############FUNCTIONS##############

def calc_frequencies(structure_id, design_dir, losses, fraction, outdir):
    '''Compare the contact similarity between designed peptide sequences
    towards the receptor.
    '''

    #Go through all design ints and calculate frequencies
    design_ints = glob.glob(design_dir+structure_id+'*_interacting_residues.csv')

    amino_acids = {'A':0, 'C':1, 'D':2, 'E':3, 'F':4, 'G':5, 'H':6, 'I':7,
                'K':8, 'L':9, 'M':10, 'N':11, 'P':12,'Q':13, 'R':14, 'S':15,
                'T':16, 'V':17, 'W':18, 'Y':19}

    #Get loss fraction
    if len(losses)>0:
        losses = losses[1:]
        selected_designs = np.argsort(losses)[:int(fraction*len(losses))]
    else:
        selected_designs = []

    freqs = {}
    for design in design_ints:
        if len(selected_designs)>0:
            if int(design.split('/')[-1].split('_')[1]) not in selected_designs:
                print(int(design.split('/')[-1].split('_')[1]))
                continue
        int_df = pd.read_csv(design)
        int_df = int_df.dropna()
        for ind, row in int_df.iterrows():
            if row.Receptor_resno in freqs:
                for aa in row.Peptide_res:
                    freqs[row.Receptor_resno][amino_acids[aa]]+=1
            else:
                freqs[row.Receptor_resno]=np.zeros(20)
                for aa in row.Peptide_res:
                    freqs[row.Receptor_resno][amino_acids[aa]]+=1

    freq_df = pd.DataFrame.from_dict(freqs, orient='index')
    #Save
    freq_df.to_csv(outdir+structure_id+'_logo_freqs.csv')
#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
structure_id = args.structure_id[0]
design_dir = args.design_dir[0]
fraction = args.fraction[0]
outdir = args.outdir[0]

try:
    losses = np.load(args.losses[0])
except:
    losses = []
calc_frequencies(structure_id, design_dir, losses, fraction, outdir)
