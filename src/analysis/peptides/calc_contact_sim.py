import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import pdb


parser = argparse.ArgumentParser(description = '''Calculate the contact similarity between the native and designed peptide sequences.''')
parser.add_argument('--native_ints', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with native interacting residues.')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--design_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory containing csvs with interacting residues for the designed peptides.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')

##############FUNCTIONS##############
def group_residues(df):
    '''Group residues
    '''
    hp = 1
    small = 2
    polar = 3
    neg = 4
    pos = 5

    AA_groups = { 'A':hp,'R':pos,'N':polar,'D':neg,'C':polar,'E':neg,
                    'Q':polar,'G':small,'H':pos,'I':hp,'L':hp,'K':pos,
                    'M':hp,'F':hp,'P':hp,'S':polar,'T':polar,'W':hp,
                    'Y':hp,'V':hp
                  }

    groupings = []
    for ind,row in df.iterrows():
        try:
            groupings.append(np.unique([AA_groups[x] for x in row.Peptide_res]))
        except:
            pdb.set_trace()
    df['Groups']=groupings    
    
    return df


def calc_similarity(native_ints, structure_id, design_dir, outdir):
    '''Compare the contact similarity between designed and native peptide sequences
    towards the receptor.
    '''
    native_ints = native_ints[['Receptor_resno', 'Peptide_res']]
    #Group the residues
    native_ints = group_residues(native_ints)
    #Get the total unique ints
    native_num_uints = sum([len(x) for x in native_ints.Groups])
    #Go through all design ints and comapare to the native
    design_ints = glob.glob(design_dir+structure_id+'*_interacting_residues.csv')
    sim_df = {'ID':[], 'frac_sim':[]}
    for design in design_ints:
        int_df = pd.read_csv(design)
        int_df = group_residues(int_df[['Receptor_resno', 'Peptide_res']].dropna())
        #Merge
        merged = pd.merge(native_ints, int_df, on='Receptor_resno', how='left')
        merged = merged.dropna()
        if len(merged)>0:
            nmatches = 0
            for ind, row in merged.iterrows():
                nmatches += np.intersect1d(row.Groups_x, row.Groups_y).shape[0]

            #Save
            sim_df['ID'].append(int(design.split('/')[-1].split('_')[1]))
            sim_df['frac_sim'].append(nmatches/native_num_uints)       
        else:
            #Save
            sim_df['ID'].append(int(design.split('/')[-1].split('_')[1]))
            sim_df['frac_sim'].append(0)
            continue


    #Create df
    sim_df = pd.DataFrame.from_dict(sim_df)
    #Save
    sim_df.to_csv(outdir+structure_id+'_contact_similarities.csv')
#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
native_ints = pd.read_csv(args.native_ints[0])
structure_id = args.structure_id[0]
design_dir = args.design_dir[0]
outdir = args.outdir[0]

calc_similarity(native_ints, structure_id, design_dir, outdir)
