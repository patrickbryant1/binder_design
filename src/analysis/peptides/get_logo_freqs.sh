#
# #LNR set
# IDS=../../data/peptide_ids_2ang.txt
# NATIVE_PDBDIR=../../data/PDB/
#
# for i in {1..12}
# do
#   ID=$(sed -n $i'p' $IDS)
#   ID=$(echo $ID|cut -d '_' -f 1)
#   echo $ID
#   python3 ./calc_contact_logo.py --structure_id $ID \
#   --design_dir $NATIVE_PDBDIR \
#   --losses $IDS \
#   --fraction 0.1 \
#   --outdir $NATIVE_PDBDIR/
# done


#Motif set
IDS=../../data/motif/ids.txt
NATIVE_PDBDIR=../../data/motif/PDB/

for i in {1..12}
do
  ID=$(sed -n $i'p' $IDS)
  echo $ID
  python3 ./calc_contact_logo.py --structure_id $ID \
  --design_dir $NATIVE_PDBDIR \
  --losses $IDS \
  --fraction 0.1 \
  --outdir $NATIVE_PDBDIR/
done
