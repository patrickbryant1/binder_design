#Native
SEQUENCE_DF=../../data/LNR_set.csv
OUTNAME=../../data/native_lm_probs.csv
#python3 ./get_esm_probs.py --sequence_df $SEQUENCE_DF --outname $OUTNAME

#Mut
#Mutated fasta to csv
# PEPTIDE_DIR=../../data/fasta/peptide/
# IDS=../../data/peptide_ids_5ang.txt
# for i in {1..20}
# do
#   ID=$(sed -n $i'p' $IDS)
#   echo $ID
#   SEQUENCE_DF=$PEPTIDE_DIR/$ID'_mut.csv'
#   OUTNAME=$PEPTIDE_DIR/$ID'_mut_lm_probs.csv'
#   python3 ./get_esm_probs.py --sequence_df $SEQUENCE_DF --outname $OUTNAME
# done

#Merge
#awk 'FNR>1' ../../data/fasta/peptide/*_mut_lm_probs.csv
