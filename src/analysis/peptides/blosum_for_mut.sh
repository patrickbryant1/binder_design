#Get the BOSUM62 scores for each mutated sequence
IDS=../../data/peptide_ids_2ang.txt
FASTADIR=../../data/fasta/peptide
BLOSUM62=../../data/BLOSUM62.csv
for i in {1..12}
do
  ID=$(sed -n $i'p' $IDS)
  SEQS=$FASTADIR/$ID'_mut.csv'
  NATIVE_SEQ=$(sed -n 2p $FASTADIR/$ID'.fasta')
  echo $ID
  python3 ./analyse_mut_seqs.py --sequences $SEQS \
  --native_seq $NATIVE_SEQ --BLOSUM62 $BLOSUM62
done
