import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
import subprocess
import pdb


parser = argparse.ArgumentParser(description = '''Calculate the relative accessible surface area of the receptor interface.''')
parser.add_argument('--pdbdir', nargs=1, type= str, default=sys.stdin, help = 'Path to pdbdir predictions')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--DSSP', nargs=1, type= str, default=sys.stdin, help = 'path to DSSP')
parser.add_argument('--if_residues', nargs=1, type= str, default=sys.stdin, help = 'IF residues for receptor')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Where to write all complexes')

##############FUNCTIONS##############
def calc_RASA(pdbfile, DSSP, if_residues):
    '''Run DSSP to get RASA
    '''

    #Max acc surface areas for each amino acid according to empirical measurements in:
    #Tien, Matthew Z et al. “Maximum allowed solvent accessibilites of residues in proteins.”
    #PloS one vol. 8,11 e80635. 21 Nov. 2013, doi:10.1371/journal.pone.0080635
    max_acc = { 'A':121,
    			'R':265,
    			'N':187,
    			'D':187,
    			'C':148,
    			'E':214,
    			'Q':214,
    			'G':97,
    			'H':216,
    			'I':195,
    			'L':191,
    			'K':230,
    			'M':203,
    			'F':228,
    			'P':154,
    			'S':143,
    			'T':163,
    			'W':264,
    			'Y':255,
    			'V':165,
    			'X':192 #Average of all other maximum surface accessibilites
    		  }

    polar_pos_neg = ['R', 'N', 'D', 'C', 'E', 'Q', 'H', 'K', 'S', 'T']

    out = subprocess.check_output([DSSP, '-i', pdbfile]).decode().split('\n')
    #Parse
    fetch=False
    rasas = []
    for line in out:
        if len(line)<1:
            continue
        if line.strip()[0]=='#':
            fetch=True
            continue
        if fetch==True:
            #Normalize acc by the max acc surface area for the specific amino acid
            residue = line[13]
            if residue == '!' or residue == '!*':
                continue
            acc_i_norm = (float(line[35:38].strip())/max_acc[residue])*100
            acc_i_norm = min(acc_i_norm, 100) #Should not be over 100 percent
            if residue in polar_pos_neg:#Don't count the polar or charged residues - these are given 0
                rasas.append(0)
            else:
                rasas.append(acc_i_norm)

    return np.array(rasas)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
pdbdir = args.pdbdir[0]
structure_id = args.structure_id[0]
DSSP = args.DSSP[0]
if_residues = np.load(args.if_residues[0])
outdir = args.outdir[0]

#Calc RASA
results = {'model_id':[], 'RASA':[]}
for name in glob.glob(pdbdir+'*.pdb'):
    RASA = calc_RASA(name, DSSP, if_residues)
    results['model_id'].append(name.split('/')[-1].split('_')[1].split('.')[0])
    results['RASA'].append(RASA[if_residues].mean())
    print(len(RASA))
#Save
df = pd.DataFrame.from_dict(results)
df.to_csv(outdir+structure_id+'_RASA.csv', index=None)
