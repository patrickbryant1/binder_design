NATIVE=../../data/PDB/6qg8.pdb
PRED=../../data/test/6qg8_pred.pdb
ID=6qg8
RC=A
PC=B
PEP_IFRES=../../data/PDB/peptide/6qg8_B_if.npy
OUTDIR=./
#Aligns on CAs and calculates RMSD towards all atoms in the peptide interface
# python3 ./calc_rmsd.py --native_structure $NATIVE \
# --prediction $PRED --structure_id $ID \
# --receptor_chain $RC \
# --peptide_chain $PC \
# --peptide_ifres $PEP_IFRES \
# --outdir $OUTDIR

#Calc distance to receptor if residues
NATIVE=../../data/PDB/6qg8.pdb
PRED=../../data/test/6qg8_pred.pdb
ID=6qg8
RC=A
REC_IFRES=../../data/PDB/receptor/6qg8_A_if.npy
OUTDIR=./
python3 ./calc_rmsd_to_receptor_if.py --native_structure $NATIVE \
--prediction $PRED --structure_id $ID \
--receptor_chain $RC \
--receptor_ifres $REC_IFRES \
--outdir $OUTDIR
