import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.SVDSuperimposer import SVDSuperimposer
from Bio import pairwise2
import pdb


parser = argparse.ArgumentParser(description = '''Calculate the loss function towards a reference structure.''')
parser.add_argument('--native_structure', nargs=1, type= str, default=sys.stdin, help = 'Path to pdb with native structure.')
parser.add_argument('--prediction_pattern', nargs=1, type= str, default=sys.stdin, help = 'Glob pattern for predicted structures')
parser.add_argument('--structure_id', nargs=1, type= str, default=sys.stdin, help = 'ID')
parser.add_argument('--receptor_chain', nargs=1, type= str, default=sys.stdin, help = 'Receptor chain in native structure')
parser.add_argument('--binder_chain', nargs=1, type= str, default=sys.stdin, help = 'Binder chain in native structure')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Where to write all scores (csv)')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''
    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}
    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    cat_model_seqs = {}
    cat_model_coords = {}
    cat_model_resno = {}
    cat_model_CA_coords = {}
    cat_model_CB_coords = {}
    cat_model_plDDT = {}
    atm_no=0
    for model in struc:
        for chain in model:
            #Save
            cat_model_seqs[chain.id]=[]
            cat_model_coords[chain.id]=[]
            cat_model_resno[chain.id]=[]
            cat_model_CA_coords[chain.id]=[]
            cat_model_CB_coords[chain.id]=[]
            cat_model_plDDT[chain.id]=[]
            #Reset res no
            res_no=0
            for residue in chain:
                res_no +=1
                res_name = residue.get_resname()
                if res_name not in [*three_to_one.keys()]:
                    continue
                for atom in residue:
                    atm_no+=1
                    if atm_no>99999:
                        print('More than 99999 atoms',pdbname)
                        return {}
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    x,y,z = atom.get_coord()
                    cat_model_coords[chain.id].append(atom.get_coord())
                    cat_model_resno[chain.id].append(res_no)
                    if atm_name=='CA':
                        cat_model_CA_coords[chain.id].append(atom.get_coord())
                        cat_model_seqs[chain.id]+=three_to_one[res_name]
                        cat_model_plDDT[chain.id].append(atom.get_bfactor())
                    if atm_name=='CB' or (res_name=='GLY' and atm_name=='CA'):
                        cat_model_CB_coords[chain.id].append(atom.get_coord())




    #Create np arrays
    for key in cat_model_seqs:
        cat_model_seqs[key] = np.array(cat_model_seqs[key])
        cat_model_coords[key] = np.array(cat_model_coords[key])
        cat_model_resno[key] = np.array(cat_model_resno[key])
        cat_model_CA_coords[key] = np.array(cat_model_CA_coords[key])
        cat_model_CB_coords[key] = np.array(cat_model_CB_coords[key])
        cat_model_plDDT[key] = np.array(cat_model_plDDT[key])

    return cat_model_seqs, cat_model_coords, cat_model_resno, cat_model_CA_coords, cat_model_CB_coords, cat_model_plDDT

def get_native_features(native_CA_coords, native_CB_coords, receptor_chain, binder_chain):
    '''Get the native interface residues and centre of mass
    '''

    #Calc 2-norm - distance between binder and interface
    mat = np.append(native_CB_coords[binder_chain], native_CB_coords[receptor_chain],axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    l1 = len(native_CB_coords[binder_chain])
    #Get interface
    contact_dists = dists[:l1,l1:] #first dimension = peptide, second = receptor
    receptor_if_res = np.argwhere(contact_dists<8)[:,1] 
    
    #Centre of mass for binder
    binder_coords = native_CA_coords[binder_chain]
    R = np.sum(binder_coords,axis=0)/(binder_coords.shape[0])
    
    return receptor_if_res, R

def get_sequence_mapping(native_seq, pred_seq):
    '''Get the mapping between the native and predicted sequences
    '''
    
    length_diff = len(pred_seq)-len(native_seq)
    if length_diff>0:
        for i in range(length_diff):
            if pred_seq[i:i+len(native_seq)]==native_seq:
                break
    
    else:
        i=0
    return i


def calc_loss(prediction_pattern, native_receptor_CA_coords, receptor_if_pos, COM, start_shift, outname):
    '''Calculate loss
    average receptor if dists
    average binder if dists
    binder plDDT
    COM distance
    '''
    results = {'id':[], 'if_dist_binder':[], 'if_dist_receptor':[], 'plddt':[], 'delta_CM':[]}

    #Superpose the receptor CAs and compare the centre of mass
    sup = SVDSuperimposer()
    for pred_name in glob.glob(prediction_pattern+'_*/unrelaxed_true.pdb'):
        pred_seqs, pred_coords, pred_resnos, pred_CA_coords, pred_CB_coords, pred_plDDT  = read_pdb(pred_name)
        #Superpose the CA coords
        sup.set(native_receptor_CA_coords, pred_CA_coords['A'][start_shift:start_shift+len(native_receptor_CA_coords)]) #(reference_coords, coords)
        sup.run()
        rot, tran = sup.get_rotran()
        #Rotate the peptide coords to match the centre of mass for the native comparison
        rotated_coords = np.dot(pred_CA_coords['B'], rot) + tran
        rotated_CM =  np.sum(rotated_coords,axis=0)/(rotated_coords.shape[0])
        delta_CM = np.sqrt(np.sum(np.square(COM-rotated_CM)))
        #Calc 2-norm - distance between peptide and interface
        mat = np.append(pred_coords['B'], pred_coords['A'][receptor_if_pos],axis=0)
        a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
        dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
        l1 = len(pred_coords['B'])
        #Get interface
        contact_dists = dists[:l1,l1:] #first dimension = peptide, second = receptor
    
        #Get the closest atom-atom distances across the receptor interface residues.
        closest_dists_binder = contact_dists[np.arange(contact_dists.shape[0]),np.argmin(contact_dists,axis=1)]
        closest_dists_receptor = contact_dists[np.argmin(contact_dists,axis=0),np.arange(contact_dists.shape[1])]
        
        #Save
        results['id'].append(pred_name.split('/')[-2])
        results['if_dist_binder'].append(closest_dists_binder.mean())
        results['if_dist_receptor'].append(closest_dists_receptor.mean())
        results['plddt'].append(pred_plDDT['B'].mean())
        results['delta_CM'].append(delta_CM )

    result_df = pd.DataFrame.from_dict(results)
    result_df.to_csv(outname)

#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
native_seqs, native_coords, native_resnos, native_CA_coords, native_CB_coords, native_plDDT  = read_pdb(args.native_structure[0]) #cat_model_seqs, cat_model_coords, cat_model_resno
prediction_pattern = args.prediction_pattern[0]
structure_id = args.structure_id[0]
receptor_chain = args.receptor_chain[0]
binder_chain = args.binder_chain[0]
outname = args.outname[0]

#Get receptor if res and binder COM
receptor_if_res, COM = get_native_features(native_CA_coords, native_CB_coords, receptor_chain, binder_chain)
#Get mapping from receptor PDB seq to fasta
native_receptor_seq = ''.join([*native_seqs[receptor_chain]])
#Load a design
pred_seqs, pred_coords, pred_resnos, pred_CA_coords, pred_CB_coords, pred_plDDT  = read_pdb(prediction_pattern+'_0/unrelaxed_true.pdb')
pred_receptor_seq = ''.join([*pred_seqs[receptor_chain]])
#Get the shift btw the native PDB and pred fasta sequence
start_shift = get_sequence_mapping(native_receptor_seq, pred_receptor_seq)
#Get atoms belonging to if res for the receptor
#Shift receptor_if_res
receptor_if_res+=start_shift+1
pred_receptor_if_pos = []
for ifr in receptor_if_res:
    pred_receptor_if_pos.extend([*np.argwhere(pred_resnos['A']==ifr)])
pred_receptor_if_pos = np.array(pred_receptor_if_pos)[:,0]

#Calc losses
calc_loss(prediction_pattern, native_CA_coords[receptor_chain], pred_receptor_if_pos, COM, start_shift, outname)
