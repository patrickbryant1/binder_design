
import sys
import os
import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import argparse
import glob
from sklearn import metrics
from scipy.stats import spearmanr
from collections import Counter


parser = argparse.ArgumentParser(description = '''Visualise''')
parser.add_argument('--esm_metrics', nargs=1, type= str, required=True, help = "Calculated eval metrics for ESM-IF1 designs")
parser.add_argument('--pmpnn_metrics', nargs=1, type= str, required=True, help = "Calculated eval metrics for proteinMPNN designs")
parser.add_argument('--outdir', nargs=1, type= str, help = 'Outdir.')



#################FUNCTIONS#################

def eval_designs(esm_metrics, pmpnn_metrics, outdir):
    """Evaluate the convergence analysis
    """


    #ESM
    esm_metrics = esm_metrics.dropna()
    esm_metrics = esm_metrics.reset_index()
    print('Total number of ESM-IF1 data points:', len(esm_metrics))
    esm_metrics['loss'] = 1/esm_metrics.plddt*esm_metrics.if_dist_binder*esm_metrics.if_dist_receptor*0.5*esm_metrics.delta_CM
    #Get unique IDs
    esm_metrics['ID'] = esm_metrics.ID+'_'+esm_metrics.target_id
    esm_metrics = esm_metrics[['plddt', 'binder_if_CA_rmsd', 'ID', 'loss']]

    #PMPNN
    pmpnn_metrics = pmpnn_metrics.dropna()
    pmpnn_metrics = pmpnn_metrics.reset_index()
    print('Total number of proteinMPNN data points:', len(pmpnn_metrics))
    pmpnn_metrics['loss'] = 1/pmpnn_metrics.plddt*pmpnn_metrics.if_dist_binder*pmpnn_metrics.if_dist_receptor*0.5*pmpnn_metrics.delta_CM
    #Get unique IDs
    pmpnn_metrics['ID'] = pmpnn_metrics.ID+'_'+pmpnn_metrics.target_id
    pmpnn_metrics = pmpnn_metrics[['plddt', 'binder_if_CA_rmsd', 'ID', 'loss']]


    joint_ids = np.intersect1d(pmpnn_metrics.ID.unique(), esm_metrics.ID.unique())
    esm_metrics = esm_metrics[esm_metrics.ID.isin(joint_ids)].reset_index()
    pmpnn_metrics = pmpnn_metrics[pmpnn_metrics.ID.isin(joint_ids)].reset_index()
    print('Number of ids:', len(joint_ids))
    print('Number of esm points:',len(esm_metrics))
    print('Number of pmpnn points:',len(pmpnn_metrics))

    ESM_inds, PMPNN_inds = {}, {}
    for id in joint_ids:
        ESM_inds[id] = esm_metrics[esm_metrics.ID==id].index
        PMPNN_inds[id] = pmpnn_metrics[pmpnn_metrics.ID==id].index

    #Go through and sample 1-100 and calculate the SR
    n_samples, SR_esm, SR_pmpnn = [], [], []
    for i in range(1,100):
        print(i)
        ns_esm, ns_pmpnn = 0, 0
        for id in joint_ids:
            #SR
            sel_esm = esm_metrics.loc[ESM_inds[id]].binder_if_CA_rmsd.values[:i]
            sel_pmpnn = pmpnn_metrics.loc[PMPNN_inds[id]].binder_if_CA_rmsd.values[:i]
            if np.argwhere(sel_esm<=2).shape[0]>0:
                ns_esm+=1
            if np.argwhere(sel_pmpnn<=2).shape[0]>0:
                ns_pmpnn+=1
        #Save
        n_samples.append(i)
        SR_esm.append(ns_esm)
        SR_pmpnn.append(ns_pmpnn)
    pdb.set_trace()

    #Plot
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('EvoBind vs ProteinMPNN')
    plt.plot(n_samples, SR_esm, color='tab:blue', label='EvoBind')
    plt.plot(n_samples, SR_pmpnn, color='tab:green', label='ProteinMPNN')
    plt.ylabel('Successful designs')
    plt.xlabel('Number of samples')
    plt.legend()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'nsamples_SR.png',dpi=500, format='png')
    plt.close()

    n_successful_esm, n_successful_pmpnn = SR_esm[-1], SR_pmpnn[-1]
    print('ESM SR=',100*n_successful_esm/len(joint_ids), '% from', n_successful_esm, 'designs.')
    print('PMPNN SR=',100*n_successful_pmpnn/len(joint_ids), '% from', n_successful_pmpnn, 'designs.')
    print('for a total of',len(joint_ids), 'designs.')


    best_esm, best_pmpnn = np.array(best_esm), np.array(best_pmpnn)
    #Plot best RMSD per design for each example
    colors = ['tab:blue', 'tab:green']
    data = [best_esm, best_pmpnn]
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    bp = ax.boxplot(data, patch_artist = True, notch=True, showfliers=False)
    for patch, color in zip(bp['boxes'], colors):
        patch.set_facecolor(color)
        patch.set_alpha(0.75)

    pdb.set_trace()


#################MAIN####################

#Parse args
args = parser.parse_args()
#Get data
esm_metrics = pd.read_csv(args.esm_metrics[0])
pmpnn_metrics = pd.read_csv(args.pmpnn_metrics[0])
outdir = args.outdir[0]
#Vis
eval_designs(esm_metrics, pmpnn_metrics, outdir)
