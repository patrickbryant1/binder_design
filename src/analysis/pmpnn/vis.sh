ESM_METRICS=../../../data/protein_MPNN/esm_if1_metrics.csv
PMPNN_METRICS=../../../data/protein_MPNN/pmpnn_metrics.csv
OUTDIR=../../../data/plots/pmpnn/
python3 ./vis.py --esm_metrics $ESM_METRICS \
--pmpnn_metrics $PMPNN_METRICS \
--outdir $OUTDIR
