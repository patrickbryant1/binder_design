import numpy as np
import time
import sys
import argparse
import pandas as pd
import glob
from Bio.PDB.PDBParser import PDBParser
from collections import Counter
import pdb

parser = argparse.ArgumentParser(description = '''Fetch the interface annotations from DSSP.''')
parser.add_argument('--dssp_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to DSSP annotations.')
parser.add_argument('--structure_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to pairwise structures.')
parser.add_argument('--crop_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to pairwise structures.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Where to write the SS states.')

#######################MAIN#######################

#Max acc surface areas for each amino acid according to empirical measurements in:
#Tien, Matthew Z et al. “Maximum allowed solvent accessibilites of residues in proteins.”
#PloS one vol. 8,11 e80635. 21 Nov. 2013, doi:10.1371/journal.pone.0080635
max_acc = { 'A':121, 'R':265, 'N':187, 'D':187, 'C':148,
			'E':214, 'Q':214, 'G':97, 'H':216, 'I':195,
			'L':191, 'K':230, 'M':203, 'F':228, 'P':154,
			'S':143, 'T':163, 'W':264, 'Y':255, 'V':165,
			'X':192 #Average of all other maximum surface accessibilites
		  }

def parse_dssp(filename):
    """Parse the DSSP info

     DSSP assigns eight states: 310-helix (represented by G), alpha-helix (H),
     pi-helix (I), helix-turn (T),
     extended beta sheet (E), beta bridge (B), bend (S) and
     other/loop (L).
    """

    ss_conv = {'G':'H', 'H':'H', 'I':'H', 'T':'H', #Heliz
                'E':'S', 'B':'S', 'S':'S', #Sheet
                ' ':'L'}

    parsed = {'chain':[], 'secondary_str':[], 'surface_acc':[], 'sequence':[]}
    fetch_lines = False #Don't fetch unwanted lines
    with open(filename, 'r') as file:
        for line in file:
            if fetch_lines == True:
                line = line.rstrip()
                chain = line[11]
                residue = line[13]
                str_i = line[16]
                acc_i = line[35:38].strip()

                if residue == '!' or residue == '!*':
                    continue
                else:
                    #Normalize acc_i by the max acc surface area for the specific amino acid
                    #Round to whole percent
                    acc_i_norm = round((float(acc_i)/max_acc[residue])*100, )
                    acc_i_norm = min(acc_i_norm, 100) #Should not be over 100 percent

                    #Save
                    parsed['chain'].append(chain)
                    parsed['secondary_str'].append(ss_conv[str_i])
                    parsed['surface_acc'].append(acc_i_norm)
                    parsed['sequence'].append(residue)

            if '#' in line:
                fetch_lines = True
                #now the subsequent lines will be fetched

    #Df
    parsed_df = pd.DataFrame.from_dict(parsed)

    return parsed_df

def read_pdb(pdbname):
    '''Read PDB
    '''

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    model_CA_coords = {}
    model_CB_coords = {}
    all_model_coords = {}
    all_model_resnos = {}
    model_seqs = {}
    model_plDDT = {}

    for model in struc:
        for chain in model:
            #Save
            model_CB_coords[chain.id]=[]
            #Save residue
            for residue in chain:
                res_name = residue.get_resname()
                if res_name not in three_to_one.keys():
                    continue
                for atom in residue:
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    #Save
                    if atom_id=='CB' or (atom_id=='CA' and res_name=='GLY'):
                        model_CB_coords[chain.id].append(atom.get_coord())




    #Convert to array
    for key in model_CB_coords:
        model_CB_coords[key] = np.array(model_CB_coords[key])

    return model_CB_coords


def get_if_ss(dssp_df, model_CB_coords, crop_df):
    """Get the interface SS matching the crop
    """
    #Get the interacting regions
    CBs_A, CBs_B = model_CB_coords['A'], model_CB_coords['B']
    mat = np.append(CBs_A, CBs_B,axis=0)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]
    dists = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    l1 = len(CBs_A)
    #Get interface
    contact_dists = dists[:l1,l1:] #first dimension = chA, second = chB

    #Get crop of A
    crop_As, cropAe = crop_df.loc[0].cs, crop_df.loc[0].ce
    #Get that interface in B
    contacts_A = np.argwhere(contact_dists[crop_As:cropAe,:]<8)
    rec_if_pos_B = np.unique(contacts_A[:,1])
    ss_rec_if_B = Counter(dssp_df[dssp_df.chain=='B'].reset_index().loc[rec_if_pos_B].secondary_str.values)
    ss_binder_if_A = Counter(dssp_df[dssp_df.chain=='A'].reset_index().loc[crop_As:cropAe].secondary_str.values)

    #Get crop of B
    crop_Bs, crop_Be = crop_df[crop_df.Chain=='B'].reset_index().loc[0].cs, crop_df[crop_df.Chain=='B'].reset_index().loc[0].ce
    #Get that interface in A
    contacts_B = np.argwhere(contact_dists[:,crop_Bs:crop_Be]<8)
    rec_if_pos_A = np.unique(contacts_B[:,1])
    ss_rec_if_A = Counter(dssp_df[dssp_df.chain=='A'].reset_index().loc[rec_if_pos_A].secondary_str.values)
    ss_binder_if_B= Counter(dssp_df[dssp_df.chain=='B'].reset_index().loc[crop_Bs:crop_Be].secondary_str.values)

    return ss_binder_if_A, ss_rec_if_B, ss_binder_if_B, ss_rec_if_A

##################MAIN###################
#Parse args
args = parser.parse_args()

dssp_dir = args.dssp_dir[0]
structure_dir = args.structure_dir[0]
crop_dir = args.crop_dir[0]
outname = args.outname[0]


#Fetch all annoattions
dssp_names = glob.glob(dssp_dir+'*.dssp')
annotations = {'ID':[],
                'ss_binder_if_A_loop':[], 'ss_binder_if_A_helix':[], 'ss_binder_if_A_sheet':[],
                'ss_rec_if_B_loop':[], 'ss_rec_if_B_helix':[], 'ss_rec_if_B_sheet':[],
                'ss_binder_if_B_loop':[], 'ss_binder_if_B_helix':[], 'ss_binder_if_B_sheet':[],
                'ss_rec_if_A_loop':[], 'ss_rec_if_A_helix':[], 'ss_rec_if_A_sheet':[],
                }

nc = 0
for name in dssp_names:
    try:
        #Get DSSP annotations
        dssp_df = parse_dssp(name)
        #Get id
        id = name.split('/')[-1].split('.')[0]
        #Read CBs (CA for GLY)
        model_CB_coords = read_pdb(structure_dir+id+'.pdb')
        #Get the crop df
        crop_df = pd.read_csv(crop_dir+id+'.csv')
        #Get the SS in the interface
        ss_binder_if_A, ss_rec_if_B, ss_binder_if_B, ss_rec_if_A = get_if_ss(dssp_df, model_CB_coords, crop_df)
        #Save
        #Crop A
        annotations['ID'].append(id)
        annotations['ss_binder_if_A_loop'].append(ss_binder_if_A.get('L',0))
        annotations['ss_binder_if_A_helix'].append(ss_binder_if_A.get('H',0))
        annotations['ss_binder_if_A_sheet'].append(ss_binder_if_A.get('S',0))
        annotations['ss_rec_if_B_loop'].append(ss_rec_if_B.get('L',0))
        annotations['ss_rec_if_B_helix'].append(ss_rec_if_B.get('H',0))
        annotations['ss_rec_if_B_sheet'].append(ss_rec_if_B.get('S',0))
        #Crop B
        annotations['ss_binder_if_B_loop'].append(ss_binder_if_B.get('L',0))
        annotations['ss_binder_if_B_helix'].append(ss_binder_if_B.get('H',0))
        annotations['ss_binder_if_B_sheet'].append(ss_binder_if_B.get('S',0))
        annotations['ss_rec_if_A_loop'].append(ss_rec_if_A.get('L',0))
        annotations['ss_rec_if_A_helix'].append(ss_rec_if_A.get('H',0))
        annotations['ss_rec_if_A_sheet'].append(ss_rec_if_A.get('S',0))
    except:
        continue

    nc+=1
    print(nc)

annotation_df = pd.DataFrame.from_dict(annotations)
#Save
annotation_df.to_csv(outname, index=None)
