
import sys
import os
import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import argparse
import glob
from sklearn import metrics
from scipy.stats import spearmanr
from collections import Counter


parser = argparse.ArgumentParser(description = '''Visualise''')
parser.add_argument('--zs_metrics', nargs=1, type= str, required=True, help = "Calculated eval metrics for zero-shot designs")
parser.add_argument('--conv100_metrics', nargs=1, type= str, required=True, help = "Calculated eval metrics for 100 designs designs")
parser.add_argument('--viterbi_metrics', nargs=1, type= str, required=True, help = "Calculated eval metrics for the Viterbi designs")
parser.add_argument('--true_binder_metrics', nargs=1, type= str, required=True, help = "Calculated eval metrics for the true binders")
parser.add_argument('--secondary_str', nargs=1, type= str, required=True, help = "Secondary structure in interfaces")
parser.add_argument('--off_target_metrics', nargs=1, type= str, required=True, help = "Metrics for off-target predictions")
parser.add_argument('--successful_designs_conv100', nargs=1, type= str, required=True, help = "The best designs for each of the successful heteromeric interfaces.")
parser.add_argument('--neff', nargs=1, type= str, required=True, help = "Number of effective sequences.")
parser.add_argument('--outdir', nargs=1, type= str, help = 'Outdir.')



#################FUNCTIONS#################
def eval_zero_shot(zs_metrics, outdir):
    '''Analyse the relationship between different
    features from the zero-shot designs.

    'if_dist_binder', 'if_dist_receptor', 'plddt', 'delta_CM',
       'binder_if_CA_rmsd', 'receptor_if_CB_rmsd', 'receptor_if_seq_recovery',
       'receptor_overall_seq_recovery', 'frac_recovered_contacts'
    '''

    zs_metrics['length'] = zs_metrics.ce-zs_metrics.cs
    zs_metrics = zs_metrics.dropna()
    zs_metrics = zs_metrics.reset_index()
    print('Total number of zs data points:', len(zs_metrics))
    print(Counter(zs_metrics.length.values))
    zs_metrics['loss'] = 1/zs_metrics.plddt*zs_metrics.if_dist_binder*zs_metrics.if_dist_receptor*0.5*zs_metrics.delta_CM
    zs_metrics['ID'] = zs_metrics['ID']+'-'+zs_metrics['target_id']
    #Get running average
    x_ra, y_ra = [], []
    div = [0.5,1,2,5,10,20,30,40,50,75,100,150,200]
    for i in range(len(div)-1):
        sel = zs_metrics[(zs_metrics.binder_if_CA_rmsd>div[i])&(zs_metrics.binder_if_CA_rmsd<=div[i+1])]
        x_ra.append((div[i]+div[i+1])/2)
        y_ra.append(sel.loss.mean())
    #Plot IF RMSD vs loss
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Loss and RMSD of ZS designs')
    plt.scatter(zs_metrics.binder_if_CA_rmsd, zs_metrics.loss,
    c=zs_metrics.frac_recovered_contacts, alpha=0.25, s=1)
    cbar = plt.colorbar()
    cbar.set_label('% recovered interface contacts')
    plt.ylabel('Loss')
    plt.xlabel('RMSD of design')
    #RA
    plt.plot(x_ra, y_ra, label='Average')
    plt.xscale('log')
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.legend()
    plt.tight_layout()
    plt.savefig(outdir+'design_loss_rmsd.png',dpi=500, format='png')
    plt.close()

    print('Spearman RMSD vs loss:',spearmanr(zs_metrics.binder_if_CA_rmsd, zs_metrics.loss)[0])


    #Sequence recovery of binders
    binder_seq_rec = []
    for ind, row in zs_metrics.iterrows():
        binder_seq_rec.append( np.mean([(a==b) for a, b in zip(row.native_binder_seq, row.designed_binder_seq)]))

    zs_metrics['binder_seq_rec'] = binder_seq_rec
    fig,ax = plt.subplots(figsize=(7.5/2.54,7.5/2.54))
    plt.title('ZS sequence recovery')
    sns.violinplot(data=zs_metrics, x='length', y='binder_seq_rec', palette='viridis')
    plt.ylabel('Sequence recovery')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'design_seqrec.png',dpi=500, format='png')
    plt.close()

    #Binder if seq recovery
    fig,ax = plt.subplots(figsize=(7.5/2.54,7.5/2.54))
    plt.title('ZS sequence recovery')
    sns.violinplot(data=zs_metrics, x='length', y='binder_if_seq_rec', palette='viridis')
    plt.ylabel('Sequence recovery')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'design_if_seqrec.png',dpi=500, format='png')
    plt.close()

    #CD
    zs_metrics['Contact density'] = zs_metrics.contacts/zs_metrics.length
    fig,ax = plt.subplots(figsize=(7.5/2.54,7.5/2.54))
    plt.title('ZS contact density')
    sns.violinplot(data=zs_metrics, x='length', y='Contact density', palette='viridis')
    plt.ylabel('Contact density')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'design_CD.png',dpi=500, format='png')
    plt.close()

    #ROC
    fig,ax = plt.subplots(figsize=(7.5/2.54,7.5/2.54))
    labels = np.zeros((len(zs_metrics)))
    successful_inds = np.argwhere(zs_metrics.binder_if_CA_rmsd.values<=2)[:,0]
    labels[successful_inds]=1
    print('Number of positives (IF RMSD<=2)',len(successful_inds))
    print('From', zs_metrics.loc[successful_inds].ID.unique().shape[0], 'unique target interfaces out of',zs_metrics.ID.unique().shape[0]) #The targets are the IDs

    fpr, tpr, threshold = metrics.roc_curve(labels, 1/zs_metrics.loss, pos_label=1)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label ='Loss AUC: %0.2f' % roc_auc, color='tab:blue')
    plt.title('ROC for design selection')
    plt.legend()
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'loss_ROC.png',dpi=500, format='png')
    plt.close()
    pdb.set_trace()
    #Get TPR and FPR 10%
    fpr_10 = np.argwhere(fpr<=0.1)[-1][0]
    print('At a FPR of 10%', 100*tpr[fpr_10],'% of structures can be selected.')
    print('Loss threshold=',threshold[fpr_10])

    #Do sel on l=10 only
    sel = zs_metrics[zs_metrics.length==10]
    sel = sel[sel.columns[2:]].reset_index()
    labels = np.zeros((len(sel)))
    successful_inds = np.argwhere(sel.binder_if_CA_rmsd.values<=2)[:,0]
    labels[successful_inds]=1
    print('Number of positives (IF RMSD<=2) using a length of 10',len(successful_inds))
    print('From', sel.loc[successful_inds].ID.unique().shape[0], 'unique target interfaces out of',sel.ID.unique().shape[0]) #The targets are the IDs
    fpr_10 = np.argwhere(fpr<=0.1)[-1][0]
    print('At a FPR of 10%', 100*tpr[fpr_10],'% of structures can be selected for length 10.')
    print('Loss threshold=',threshold[fpr_10])

    #Plot SR vs length
    sel = zs_metrics.loc[successful_inds]
    successful_counts = Counter(sel.length)
    all_counts = Counter(zs_metrics.length)
    success_rates = []
    for l in zs_metrics.length.unique():
        success_rates.append(successful_counts[l]/all_counts[l])

    fig,ax = plt.subplots(figsize=(6/2.54,6/2.54))
    plt.scatter(zs_metrics.length.unique(), np.array(success_rates)*100, marker='x')
    plt.title('ZS success rates')
    plt.xlabel('Length')
    plt.ylabel('Success rate (%)')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.xticks([10,20,30,40,50])
    plt.tight_layout()
    plt.savefig(outdir+'length_zs_sr.png',dpi=300, format='png')
    plt.close()

    print('Success rates:', success_rates)

    #Plot 'receptor_if_seq_recovery'vs loss
    #Get the relative receptor_if_seq_recovery
    # rel_receptor_if_seq_recovery = []
    # for ind, row in zs_metrics.iterrows():
    #     rel_receptor_if_seq_recovery.append(row.receptor_if_seq_recovery-zs_metrics[zs_metrics.ID==row.ID].receptor_if_seq_recovery.min())
    #
    # zs_metrics['rel_receptor_if_seq_recovery'] = rel_receptor_if_seq_recovery
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Receptor sequence recovery')
    plt.scatter(zs_metrics.receptor_if_seq_recovery, zs_metrics.binder_if_CA_rmsd, alpha=0.25, s=1)
    plt.xlabel('Receptor interface sequence recovery')
    plt.ylabel('RMSD')
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'receptor_if_seq_recovery_rmsd.png',dpi=500, format='png')
    plt.close()

    print('Spearman receptor if seq recovery vs if RMSD:',spearmanr(zs_metrics.receptor_if_seq_recovery, zs_metrics.binder_if_CA_rmsd)[0])


    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Receptor sequence recovery')
    plt.scatter(zs_metrics.receptor_overall_seq_recovery, zs_metrics.binder_if_CA_rmsd, alpha=0.25, s=1)
    plt.xlabel('Receptor overall sequence recovery')
    plt.ylabel('RMSD')
    #plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'receptor_overall_seq_recovery_rmsd.png',dpi=500, format='png')
    plt.close()

    #Plot contacts vs rmsd
    #zs_metrics['contacts'] = np.log(zs_metrics.contacts)
    #zs_metrics['binder_if_CA_rmsd'] = np.log(zs_metrics.binder_if_CA_rmsd)
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Contacts and RMSD of designs')
    #sns.jointplot(data=zs_metrics, x='contacts', y='binder_if_CA_rmsd', kind='kde')
    plt.scatter(zs_metrics.contacts, zs_metrics.binder_if_CA_rmsd, alpha=0.25, s=1)
    plt.xlabel('Number of native contacts in seed')
    plt.ylabel('RMSD')
    #plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'contacts_rmsd.png',dpi=500, format='png')
    plt.close()

    print('Spearman contacts cs if RMSD:',spearmanr(zs_metrics.contacts, zs_metrics.binder_if_CA_rmsd)[0])

    #Plot length and loss
    zs_metrics['loss'] = np.log(zs_metrics['loss'])
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Loss and length of designs')
    sns.violinplot(data=zs_metrics, x='length', y='loss')
    plt.ylabel('log Loss')
    plt.xlabel('Length of design')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'design_loss_length.png',dpi=500, format='png')
    plt.close()

     #Plot binder RMSD vs receptor RMSD
    zs_metrics['loss'] = np.log(zs_metrics['loss'])
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('RMSD of recptor interface')
    #sns.kdeplot(data =zs_metrics, x='binder_if_CA_rmsd', y='receptor_if_CB_rmsd')
    plt.scatter(zs_metrics.binder_if_CA_rmsd, zs_metrics.receptor_if_CB_rmsd, s=1, alpha=0.2)
    plt.xlabel('Design RMSD')
    plt.ylabel('Receptor RMSD')
    plt.xscale('log')
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'receptor_rmsd.png',dpi=500, format='png')
    plt.close()

def eval_conv100(conv100_metrics, outdir):
    """Evaluate the convergence analysis
    """


    #Get loss
    conv100_metrics = conv100_metrics.dropna()
    conv100_metrics = conv100_metrics.reset_index()
    print('Total number of zs data points:', len(conv100_metrics))
    conv100_metrics['loss'] = 1/conv100_metrics.plddt*conv100_metrics.if_dist_binder*conv100_metrics.if_dist_receptor*0.5*conv100_metrics.delta_CM
    #Get unique IDs
    conv100_metrics['ID'] = conv100_metrics.ID+'_'+conv100_metrics.target_id
    #Plot the best per ID
    best_designs = conv100_metrics.groupby(by='ID').binder_if_CA_rmsd.min()
    #SR
    print('SR=', np.round(100*np.argwhere(best_designs.values<=2).shape[0]/len(best_designs.values),2),'%')
    print('for',np.argwhere(best_designs.values<=2).shape[0],'out of', len(best_designs.values))

    #Compare the plDDT for the successful ID  (RMSD vs plDDT).
    successful_ids = conv100_metrics[conv100_metrics.binder_if_CA_rmsd<=2].ID.unique()
    successful_data_points = conv100_metrics[conv100_metrics.ID.isin(successful_ids)]
    #Create a running average
    x_ra, y_ra = [], []
    ss=0.1
    for i in np.arange(0,1,ss):
        sel = successful_data_points[(successful_data_points.binder_if_seq_rec>i)&(successful_data_points.binder_if_seq_rec<=i+ss)]
        if len(sel)>0:
            x_ra.append(i+ss/2)
            y_ra.append(sel.plddt.mean())
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    #sns.kdeplot(data=successful_data_points, x='binder_if_seq_rec', y='plddt',fill=True)
    plt.plot(x_ra, y_ra, color='tab:green')
    plt.title('Binder sequence recovery')
    plt.ylabel('plDDT')
    plt.xlabel('Binder interface sequence recovery')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'plddt_if_seq_rec.png',dpi=500, format='png')
    plt.close()

    pdb.set_trace()
    #Plot loss vs RMSD
    #Get running average
    x_ra, y_ra = [], []
    div = [0,0.5,1,2,5,10,20,30,40,50,75,100,150,200]
    for i in range(len(div)-1):
        sel = conv100_metrics[(conv100_metrics.binder_if_CA_rmsd>div[i])&(conv100_metrics.binder_if_CA_rmsd<=div[i+1])]
        x_ra.append((div[i]+div[i+1])/2)
        y_ra.append(sel.loss.mean())
    R, p = spearmanr(conv100_metrics.binder_if_CA_rmsd, conv100_metrics.loss)
    print('Spearman loss and RMSD =',np.round(R,2))
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.scatter(conv100_metrics.binder_if_CA_rmsd, conv100_metrics.loss, s=1, alpha=0.05, c=conv100_metrics.frac_recovered_contacts)
    #RA
    plt.plot(x_ra, y_ra, label='Average')
    plt.title('Loss and RMSD of \nconvergence analysis')
    #cbar = plt.colorbar()
    #cbar.set_label('% recovered interface contacts')
    plt.legend()
    plt.ylabel('Loss')
    plt.xlabel('RMSD')
    plt.xscale('log')
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'conv_loss_rmsd.png',dpi=500, format='png')
    plt.close()

    #Select using loss threshold cutoff
    cutoff=1
    sel = conv100_metrics[(conv100_metrics.loss<cutoff)&(conv100_metrics.plddt>80)]
    #SR
    print('Using the cutoff', cutoff,'for selection. SR=', np.round(100*np.argwhere(sel.binder_if_CA_rmsd.values<=2).shape[0]/len(sel.values),2),'%')
    print('for', len(sel.ID.unique()),'unique IDs.')

    #Sequence similarity

    #Get successful examples
    for id in conv100_metrics[conv100_metrics.binder_if_CA_rmsd<=2].ID.unique():
        sel = conv100_metrics[conv100_metrics.ID==id]
        sel = sel.reset_index()
        if sel.plddt.values[np.argmin(sel.binder_if_CA_rmsd)]>80:
            print(id, 'Selected on RMSD:',np.argmin(sel.binder_if_CA_rmsd), 'with plDDT:',sel.plddt.values[np.argmin(sel.binder_if_CA_rmsd)])


    #Sample per ID
    #Get the indices for each id
    # ID_success = {}
    # for id in conv100_metrics.ID.unique():
    #     sel = conv100_metrics[conv100_metrics.ID==id]
    #     success_inds = np.zeros((sel.shape[0]))
    #     success_inds[np.argwhere(sel.binder_if_CA_rmsd.values<=2)[:,0]]=1
    #     ID_success[id]=success_inds
    #
    # #Go through and sample
    # y = np.zeros((100)) #mean
    # for i in range(1,101):
    #     print(i)
    #     n_successful = 0
    #     for id in ID_success:
    #         sel = ID_success[id][:i]
    #         if np.argwhere(sel==1).shape[0]>0:
    #             n_successful+=1
    #     #Add the success rate
    #     y[i-1] = n_successful
    #
    # #Normalise
    # y /= len(ID_success.keys())
    # #Plot
    # fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    # plt.title('SR and sampling')
    # plt.plot(np.arange(1,len(y)+1), 100*y)
    # plt.xlabel('Number of samples')
    # plt.ylabel('Success rate (%)')
    # ax.spines['top'].set_visible(False)
    # ax.spines['right'].set_visible(False)
    # plt.tight_layout()
    # plt.savefig(outdir+'sr_nsamples.png',dpi=500, format='png')
    # plt.close()

def plot_conv100_contact_rmsd(conv100_metrics, zs_metrics, outdir):
    """Plot the contact density vs the RMSD of the designs
    """

    zs_metrics['ID'] = zs_metrics.ID+'-'+zs_metrics.target_id
    zs_metrics['length'] = zs_metrics.ce-zs_metrics.cs
    #Select on length
    zs_metrics = zs_metrics[zs_metrics.length==10]
    zs_metrics = zs_metrics.dropna()
    zs_metrics = zs_metrics.reset_index()
    #Contact density
    zs_metrics['CD']=zs_metrics.contacts/zs_metrics.length
    zs_metrics = zs_metrics[['ID', 'CD']]

    conv100_metrics['ID'] = conv100_metrics.ID+'-'+conv100_metrics.target_id
    conv100_metrics = conv100_metrics.dropna()
    conv100_metrics = conv100_metrics.reset_index()
    merged = pd.merge(conv100_metrics, zs_metrics, on='ID', how='left')

    #Get the SR for different densities
    merged = merged.dropna()
    merged = merged.sort_values(by='CD')
    merged = merged[merged.columns[1:]]
    merged = merged.reset_index()
    CD, SR = [], []
    num_thresholds = 30
    ss = int(len(merged)/num_thresholds)
    print(ss,'data points in a total number of',num_thresholds,'partitions')
    for i in range(0,len(merged),ss):
        sel = merged.loc[i:i+ss]
        CD.append(sel.CD.mean())
        SR.append(sel[sel.binder_if_CA_rmsd<=2].shape[0]/len(sel))

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Contact density and sucess rate')
    plt.plot(CD, SR)
    plt.xlabel('Contact density')
    plt.ylabel('Success rate')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'CD_SR_conv100.png',dpi=500, format='png')
    plt.close()
    pdb.set_trace()


def eval_viterbi(viterbi_metrics, conv100_metrics, outdir):
    """Evaluate the Viterbi designs
    """

    #Get loss
    conv100_metrics = conv100_metrics.dropna()
    conv100_metrics = conv100_metrics.reset_index()
    conv100_metrics['loss'] = 1/conv100_metrics.plddt*conv100_metrics.if_dist_binder*conv100_metrics.if_dist_receptor*0.5*conv100_metrics.delta_CM
    #Get unique IDs
    conv100_metrics['ID'] = conv100_metrics.ID+'_'+conv100_metrics.target_id
    #Plot the best per ID
    best_designs = conv100_metrics.groupby(by='ID').binder_if_CA_rmsd.min()
    best_design_df = pd.DataFrame()
    best_design_df['ID'] = [*best_designs.keys()]
    best_design_df['RMSD'] = [*best_designs.values]

    viterbi_metrics = viterbi_metrics.dropna()
    viterbi_metrics = viterbi_metrics.reset_index()
    viterbi_metrics['loss'] = 1/viterbi_metrics.plddt*viterbi_metrics.if_dist_binder*viterbi_metrics.if_dist_receptor*0.5*viterbi_metrics.delta_CM
    #Get unique IDs
    viterbi_metrics['ID'] = viterbi_metrics.ID+'_'+viterbi_metrics.target_id
    #Merge
    merged = pd.merge(viterbi_metrics, best_design_df, on='ID')
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Viterbi vs all sampling')
    plt.scatter(merged.RMSD, merged.binder_if_CA_rmsd, alpha=0.5, s=1, color='tab:orange', marker='+')
    plt.plot([0,2],[2,2], color='grey', linestyle='--')
    plt.plot([2,2],[0,2], color='grey',linestyle='--')
    plt.plot([2,100], [2,100], color='grey', linestyle='--')
    plt.xlabel('Best RMSD all sampling')
    plt.ylabel('Viterbi RMSD')
    plt.xscale('log')
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'viterbi_conv_rmsd.png',dpi=500, format='png')
    plt.close()


def true_vs_conv100(true_binder_metrics, conv100_metrics, outdir):
    """Evaluate the true vs the best from the conv100
    """

    conv100_metrics = conv100_metrics.dropna()
    conv100_metrics = conv100_metrics.reset_index()
    conv100_metrics['loss'] = 1/conv100_metrics.plddt*conv100_metrics.if_dist_binder*conv100_metrics.if_dist_receptor*0.5*conv100_metrics.delta_CM
    #Get IDs
    conv100_metrics['ID'] = conv100_metrics.ID+'_'+conv100_metrics.target_id
    best_designs = conv100_metrics.groupby(by='ID').binder_if_CA_rmsd.min()
    best_design_df = pd.DataFrame()
    best_design_df['ID'] = [*best_designs.keys()]
    best_design_df['RMSD'] = [*best_designs.values]

    true_binder_metrics = true_binder_metrics.dropna()
    true_binder_metrics = true_binder_metrics.reset_index()
    true_binder_metrics['loss'] = 1/true_binder_metrics.plddt*true_binder_metrics.if_dist_binder*true_binder_metrics.if_dist_receptor*0.5*true_binder_metrics.delta_CM
    #Get IDs
    true_binder_metrics['ID'] = true_binder_metrics.ID+'_'+true_binder_metrics.target_id


    #Merge
    merged = pd.merge(best_design_df, true_binder_metrics, on='ID')

    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.title('Native vs best design')
    plt.scatter(merged.RMSD, merged.binder_if_CA_rmsd, alpha=0.5, s=1, color='tab:blue')
    plt.plot([0,100], [0,100], color='grey', linestyle='--')
    plt.xlabel('Best RMSD all sampling')
    plt.ylabel('Native sequence RMSD')
    plt.xscale('log')
    plt.yscale('log')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'true_conv_rmsd.png',dpi=500, format='png')
    plt.close()

def ss_analysis(secondary_str, conv100_metrics, outdir):
    """Analyse the RMSD of the design vs the seed secondary structure interface

    'ss_binder_if_A_loop', 'ss_binder_if_A_helix',
    'ss_binder_if_A_sheet', 'ss_rec_if_B_loop', 'ss_rec_if_B_helix',
    'ss_rec_if_B_sheet', 'ss_binder_if_B_loop', 'ss_binder_if_B_helix',
    'ss_binder_if_B_sheet', 'ss_rec_if_A_loop', 'ss_rec_if_A_helix',
    'ss_rec_if_A_sheet'

    """

    conv100_metrics = conv100_metrics.dropna()
    conv100_metrics = conv100_metrics.reset_index()
    conv100_metrics['loss'] = 1/conv100_metrics.plddt*conv100_metrics.if_dist_binder*conv100_metrics.if_dist_receptor*0.5*conv100_metrics.delta_CM
    #Get IDs
    conv100_metrics['ID'] = conv100_metrics.ID+'_'+conv100_metrics.target_id
    best_designs = conv100_metrics.groupby(by='ID').binder_if_CA_rmsd.min()
    best_design_df = pd.DataFrame()
    best_design_df['ID'] = [*best_designs.keys()]
    best_design_df['RMSD'] = [*best_designs.values]

    #Order the ss
    best_design_df['ch1'] = [x.split('-')[0].split('_')[1] for x in best_design_df.ID]
    best_design_df['ch2'] = [x.split('-')[1].split('_')[1] for x in best_design_df.ID]
    best_design_df['target_chain'] = [x.split('-')[1].split('_')[3] for x in best_design_df.ID]
    best_design_df['ID'] = [x[:-7] for x in best_design_df.ID]
    #Merge
    merged = pd.merge(best_design_df, secondary_str, on='ID')
    #Sort out the corresponding binders
    sorted_ss = {'ss_binder_if_loop':[], 'ss_binder_if_helix':[],
                'ss_binder_if_sheet':[], 'ss_rec_if_loop':[],
                'ss_rec_if_helix':[], 'ss_rec_if_sheet':[]}
    for ind, row in merged.iterrows():
        if row.target_chain==row.ch2:
            binder='A'
            receptor='B'
        else:
            binder='B'
            receptor='A'
        #Save
        sorted_ss['ss_binder_if_loop'].append(row['ss_binder_if_'+binder+'_loop'])
        sorted_ss['ss_binder_if_helix'].append(row['ss_binder_if_'+binder+'_helix'])
        sorted_ss['ss_binder_if_sheet'].append(row['ss_binder_if_'+binder+'_sheet'])
        sorted_ss['ss_rec_if_loop'].append(row['ss_rec_if_'+receptor+'_loop'])
        sorted_ss['ss_rec_if_helix'].append(row['ss_rec_if_'+receptor+'_helix'])
        sorted_ss['ss_rec_if_sheet'].append(row['ss_rec_if_'+receptor+'_sheet'])


    merged = merged[merged.columns[:5]]
    for key in sorted_ss:
        merged[key]=sorted_ss[key]

    #Invert RMSD
    merged['RMSD'] = 1/merged.RMSD
    nicer_names = {'ss_rec_if_loop':'Loop', 'ss_rec_if_helix':'Helix', 'ss_rec_if_sheet':'Sheet'}
    for type in ['ss_rec_if_loop', 'ss_rec_if_helix', 'ss_rec_if_sheet']:
        fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
        merged[type] = merged[type]/max(merged[type])
        R,p = spearmanr(merged[type], merged.RMSD)
        sns.kdeplot(data=merged, x = type, y='RMSD', fill=True)
        plt.title(nicer_names[type]+' vs RMSD')
        plt.xlabel('% '+nicer_names[type])
        plt.ylabel('1/RMSD')
        plt.ylim([-0.25,1.5])
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        plt.tight_layout()
        plt.savefig(outdir+type+'_vs_rmsd.png',dpi=500, format='png')
        plt.close()


def neff_vs_success(neff, conv100_metrics, outdir):
    """Analyse the relationship between Neff and the success rate
    """

    #Remove NaNs
    conv100_metrics = conv100_metrics.dropna()
    conv100_metrics = conv100_metrics.reset_index()
    #Neff
    neff = [x.split() for x in neff[0]]
    neff_df = pd.DataFrame()
    neff_df['neff'] = [int(x[0]) for x in neff]
    neff_df['target_id'] = [x[1].split('/')[10] for x in neff]

    pdb.set_trace()
    #Merge
    merged = pd.merge(conv100_metrics, neff_df, on='target_id', how='left')
    merged = merged.dropna()

    ss=1000
    nf, sr = [], []
    for i in np.arange(0,merged.neff.max(),ss):
        sel = merged[(merged.neff>i)&(merged.neff<=i+ss)]
        if len(sel)>0:
            nf.append(i+ss/2)
            sr.append(sel[sel.binder_if_CA_rmsd<=2].shape[0]/len(sel))

    #Plot
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    #plt.plot(nf, sr)
    sns.kdeplot(data=merged, x='neff', y='binder_if_CA_rmsd', fill=True, cmap='Oranges')
    plt.title('Neff and RMSD')
    plt.xlabel('Neff')
    plt.ylabel('RMSD')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'neff_vs_rmsd.png',dpi=500, format='png')
    plt.close()


def get_successful_designs(conv100_metrics):
    """Get the successful designs from the conv100 analysis
    """

    conv100_metrics = conv100_metrics.dropna()
    conv100_metrics = conv100_metrics.reset_index()
    conv100_metrics['loss'] = 1/conv100_metrics.plddt*conv100_metrics.if_dist_binder*conv100_metrics.if_dist_receptor*0.5*conv100_metrics.delta_CM
    #Get IDs
    conv100_metrics['ID'] = conv100_metrics.ID+'_'+conv100_metrics.target_id
    #Save the successful ids
    success_metrics = {'ID':[], 'Target':[], 'Designed_sequence':[], 'binder_if_CA_rmsd':[], 'plddt':[]}
    for id in conv100_metrics.ID.unique():
        sel = conv100_metrics[conv100_metrics.ID==id]
        if sel.binder_if_CA_rmsd.min()<=2:
            sel = sel.reset_index()
            #Get design df
            design_df = pd.read_csv('../../../data/Pfam/PDB/conv_100/'+id[:-7]+'.csv')
            #Get seqs
            if sel.target_id.values[0]==id[:6]: #This means the target is the first chain
                #The second row should then be selected
                designed_seq = design_df.loc[1].designed_binder_seq.split('-')[np.argmin(sel.binder_if_CA_rmsd)]
            else:
                designed_seq = design_df.loc[0].designed_binder_seq.split('-')[np.argmin(sel.binder_if_CA_rmsd)]
            #Save
            success_metrics['ID'].append(id)
            success_metrics['Target'].append(sel.target_id.values[0])
            success_metrics['Designed_sequence'].append(designed_seq)
            success_metrics['binder_if_CA_rmsd'].append(sel.binder_if_CA_rmsd.min())
            success_metrics['plddt'].append(sel.plddt.values[np.argmin(sel.binder_if_CA_rmsd)])

    #Save
    successful_design_df = pd.DataFrame.from_dict(success_metrics)
    successful_design_df.to_csv('../../../data/Pfam/successful_designs_conv100.csv', index=None)
    #Get the unsuccessful ids
    unsuccessful_targets = np.setdiff1d(conv100_metrics.target_id.unique(), successful_design_df.Target.values)
    unsuccessful_targets = np.unique(unsuccessful_targets)
    unsuccessful_targets = unsuccessful_targets[np.random.choice(len(unsuccessful_targets),100, replace=False)]
    unsuccessful_target_df = pd.DataFrame()
    unsuccessful_target_df['Target'] = unsuccessful_targets
    unsuccessful_target_df.to_csv('../../../data/Pfam/unsuccessful_designs_sel100.csv', index=None)


def analyse_off_target(off_target_metrics, successful_designs_conv100, outdir):
    """Analyse the off-target predictions and compare
    with the plddt of the intended targets
    """


    #Merge
    merged = pd.merge(successful_designs_conv100, off_target_metrics, left_on='Target', right_on='True_target')
    #Get medians
    print('On-target median plDDT=',merged.plddt_x.median())
    print('Off-target median plDDT=',merged.plddt_y.median())
    #Plot
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    sns.kdeplot(data=merged, x = 'plddt_x', y='plddt_y', fill=True)
    plt.plot([0,100],[0,100], color='grey', linestyle='--')
    plt.title('On- vs Off-target plDDT')
    plt.xlabel('On-target plDDT')
    plt.ylabel('Off-target plDDT')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'off_target_plddt.png',dpi=500, format='png')
    plt.close()

    #ROC
    delta_plddt = merged.plddt_x.values-merged.plddt_y.values
    delta_plddt = np.concatenate([np.zeros(len(successful_designs_conv100)), delta_plddt])+1e-6
    delta_plddt[delta_plddt<0] = 1e-6
    labels = np.zeros(delta_plddt.shape)
    labels[:len(successful_designs_conv100)]=1
    fpr, tpr, threshold = metrics.roc_curve(labels, 1/delta_plddt, pos_label=1)
    roc_auc = metrics.auc(fpr, tpr)
    fig,ax = plt.subplots(figsize=(9/2.54,9/2.54))
    plt.plot(fpr, tpr, label ='plDDT AUC: %0.2f' % roc_auc, color='tab:blue')
    plt.title('ROC for on-target selection')
    plt.legend()
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    plt.tight_layout()
    plt.savefig(outdir+'off_target_ROC.png',dpi=500, format='png')
    plt.close()

    fpr_10 = np.argwhere(fpr<=0.1)[-1][0]
    print('At a FPR of 10%', 100*tpr[fpr_10],'% of designs can be selected.')
    print('Loss threshold=',threshold[fpr_10])
    pdb.set_trace()


#################MAIN####################

#Parse args
args = parser.parse_args()
#Get data
zs_metrics = pd.read_csv(args.zs_metrics[0])
conv100_metrics = pd.read_csv(args.conv100_metrics[0])
viterbi_metrics = pd.read_csv(args.viterbi_metrics[0])
true_binder_metrics = pd.read_csv(args.true_binder_metrics[0])
secondary_str = pd.read_csv(args.secondary_str[0])
off_target_metrics = pd.read_csv(args.off_target_metrics[0])
successful_designs_conv100 = pd.read_csv(args.successful_designs_conv100[0])
neff = pd.read_csv(args.neff[0], header=None)
outdir = args.outdir[0]

#Eval zero-shot metrics
#eval_zero_shot(zs_metrics, outdir)
#Eval the conv100 metrics
#eval_conv100(conv100_metrics, outdir)
#Contact density and RMSD
#plot_conv100_contact_rmsd(conv100_metrics, zs_metrics, outdir)
#Eval Viterbi
#eval_viterbi(viterbi_metrics, conv100_metrics, outdir)
#True vs best from conv
#true_vs_conv100(true_binder_metrics, conv100_metrics, outdir)
#Analyse the secondary_str vs the RMSD
ss_analysis(secondary_str, conv100_metrics, outdir)
#Neff vs success rate
#neff_vs_success(neff, conv100_metrics, outdir)

#Get successful designs for off-target analysis
#get_successful_designs(conv100_metrics)
#analyse_off_target(off_target_metrics, successful_designs_conv100, outdir)
pdb.set_trace()
