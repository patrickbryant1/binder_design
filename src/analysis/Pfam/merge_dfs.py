import argparse
import sys
import os

import pandas as pd
import glob
import pdb


parser = argparse.ArgumentParser(description = '''Calculate the loss function towards a reference structure.''')
parser.add_argument('--metric_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to dir with csv files with metrics.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Where to write all scores (csv)')




#################MAIN####################

#Parse args
args = parser.parse_args()
outname = args.outname[0]



i=0
filenames = glob.glob(args.metric_dir[0]+'*.csv')
merged = pd.read_csv(filenames[0])
for filename in filenames[1:]:
    i+=1
    print(i)
    df = pd.read_csv(filename)
    #Add
    merged = pd.concat([merged,df])

#Save
pdb.set_trace()
merged.to_csv(outname, index=None)
