ZS_METRICS=../../../data/Pfam/zs_metrics_gap.csv
CONV100_METRICS=../../../data/Pfam/conv_100_metrics.csv
VITERBI=../../../data/Pfam/viterbi_metrics.csv
TRUE_BINDERS=../../../data/Pfam/true_binder_metrics.csv
SS=../../../data/Pfam/if_dssp.csv
OFF_TARGET=../../../data/Pfam/off_target_metrics.csv
SUCC_DESIGNS_CONV100=../../../data/Pfam/successful_designs_conv100.csv
NEFF=../../../data/Pfam/neff.txt
OUTDIR=../../../data/plots/Pfam/
python3 ./vis.py --zs_metrics $ZS_METRICS \
--conv100_metrics $CONV100_METRICS \
--viterbi_metrics $VITERBI \
--true_binder_metrics $TRUE_BINDERS \
--secondary_str $SS \
--off_target_metrics $OFF_TARGET \
--successful_designs_conv100 $SUCC_DESIGNS_CONV100 \
--neff $NEFF \
--outdir $OUTDIR
