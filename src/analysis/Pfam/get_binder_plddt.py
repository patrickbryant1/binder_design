import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob
from collections import defaultdict
from Bio.PDB.PDBParser import PDBParser
from Bio.SVDSuperimposer import SVDSuperimposer
from Bio import pairwise2
import pdb


parser = argparse.ArgumentParser(description = '''Calculate the average binder plDDT.''')
parser.add_argument('--pred_dir', nargs=1, type= str, default=sys.stdin, help = 'Path to predicted pdbs.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Where to write all scores (csv)')

##############FUNCTIONS##############
def read_pdb(pdbname):
    '''Read PDB
    '''

    three_to_one = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V',
    'SEC':'U', 'PYL':'O', 'GLX':'X', 'UNK': 'X'}

    parser = PDBParser()
    struc = parser.get_structure('', pdbname)

    #Save
    model_CA_coords = {}
    model_CB_coords = {}
    all_model_coords = {}
    all_model_resnos = {}
    model_seqs = {}
    model_plDDT = {}

    for model in struc:
        for chain in model:
            #Save
            all_model_coords[chain.id]=[]
            model_CA_coords[chain.id]=[]
            model_CB_coords[chain.id]=[]
            all_model_resnos[chain.id]=[]
            model_seqs[chain.id]=[]
            model_plDDT[chain.id]=[]
            #Save residue
            for residue in chain:
                res_name = residue.get_resname()
                if res_name not in three_to_one.keys():
                    continue
                for atom in residue:
                    atom_id = atom.get_id()
                    atm_name = atom.get_name()
                    #Save
                    all_model_coords[chain.id].append(atom.get_coord())
                    all_model_resnos[chain.id].append(residue.get_id()[1])
                    if atom_id=='CA':
                        model_CA_coords[chain.id].append(atom.get_coord())
                        model_seqs[chain.id].append(three_to_one[res_name])
                        model_plDDT[chain.id].append(atom.get_bfactor())
                    if atom_id=='CB' or (atom_id=='CA' and res_name=='GLY'):
                        model_CB_coords[chain.id].append(atom.get_coord())




    #Convert to array
    for key in model_CA_coords:
        all_model_coords[key] = np.array(all_model_coords[key])
        model_CA_coords[key] = np.array(model_CA_coords[key])
        model_CB_coords[key] = np.array(model_CB_coords[key])
        all_model_resnos[key] = np.array(all_model_resnos[key])
        model_seqs[key] = np.array(model_seqs[key])
        model_plDDT[key] = np.array(model_plDDT[key])

    return all_model_coords, model_CA_coords, model_CB_coords, all_model_resnos, model_seqs, model_plDDT






#################MAIN####################

#Parse args
args = parser.parse_args()
#Data
pred_dir = args.pred_dir[0]
outname = args.outname[0]


results = {'Target':[], 'plddt':[]}

for name in glob.glob(pred_dir+'*.pdb'):
    all_model_coords, model_CA_coords, model_CB_coords, all_model_resnos, model_seqs, model_plDDT = read_pdb(name)
    #Save
    target_id = '_'.join(name.split('/')[-1].split('_')[1:3])
    results['Target'].append(target_id)
    results['plddt'].append(model_plDDT['B'].mean())

#Df
results_df = pd.DataFrame.from_dict(results)
#Add true target id
results_df['True_target'] = outname.split('/')[-1][:6]
#Save
results_df.to_csv(outname, index=None)

