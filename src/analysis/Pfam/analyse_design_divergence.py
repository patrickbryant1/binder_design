
import sys
import os
import pdb

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import argparse
import glob
from collections import Counter


parser = argparse.ArgumentParser(description = '''Analyse the sequence divergence of the designs''')
parser.add_argument('--design_dir', nargs=1, type= str, required=True, help = "Directory containing csvs with designed seqs")

#################FUNCTIONS#################
def analyse_div(design_dir):
    """Analyse the divergence
    """

    designs = glob.glob(design_dir+'*.csv')

    results = {'n_unique':[]}
    i=0
    for design in designs:
        design_df = pd.read_csv(design)

        seqs = [x.split('-') for x in design_df.designed_binder_seq.values]
        counts = [Counter(x) for x in seqs]
        #Save
        #results['temp'].append(float(design.split('/')[-1].split('_')[-1][:-4]))
        results['n_unique'].append(np.average([len(x.keys()) for x in counts]))
        i+=1
        print(i,'out of',len(designs))

    #df
    results_df = pd.DataFrame.from_dict(results)
    print('Fraction unique:')
    print(results_df.n_unique.mean())
    #print(results_df.groupby('temp').mean()/10)


#################MAIN####################

#Parse args
args = parser.parse_args()
#Get data
design_dir = args.design_dir[0]

#Analyse the sequences divergence in the designs
analyse_div(design_dir)
